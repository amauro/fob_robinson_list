usage() {
  echo ""
  echo "Usage: $0 -a parameterA -b parameterB -c parameterC"
  echo -e "\t-a Iteration test (Mandatory)"
  echo -e "\t-b Mobile phone to query (Mandatory)"
  echo "Test query project"
}

while getopts ':a:b:' opt; do
    case $opt in
    a) iterations="$OPTARG" ;;
    b) mobile="$OPTARG" ;;
    h)
      usage
      exit 0
      ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        usage >&2
        exit 2
        ;;
    esac
done

echo "$iterations"
echo "$mobile"

for ((i = 1; i <= "$iterations"; i++))
do
  echo "Welcome $i times"
  echo 'Run db test'
  docker-compose exec -T django-web python manage.py prune_user_test -n $mobile --db -f "testdb_$i";
  echo 'Run blockchain test'
  docker-compose exec -T django-web python manage.py prune_user_test -n $mobile -f "testbc_$i";
done