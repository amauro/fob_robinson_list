"""ClubUse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from clubuse_ui import views
from django.contrib.auth import views as auth_views
from clubuse_ui.views import log_view, ratelimit
import clubuse_ui.forms

from rest_framework.schemas import get_schema_view


def log_sec(view, **kwargs):
    kwargs.setdefault("log", "security")
    return log_view(view, **kwargs)


urlpatterns = [
    path("", views.index, name="index"),
    path("account_info", views.account_info, name="account_info"),
    path("transactions", views.TransactionsView.as_view(),
         name="transactions"),
    path("payload", views.readPayload, name="readpayload"),
    path("admin/", admin.site.urls),
    path("accedi", views.index, name="accedi"),
    path("register", views.RegisterView.as_view(), name="register"),
    path("opt_in", views.opt_in, name="opt_in"),
    path("opt_out", views.opt_out, name="opt_out"),
    path("confronta_csv", views.confronta_csv, name="confronta_csv"),
    path("optin_user_asa", views.optin_user_asa, name="optin_user_asa"),
    path("process_op", views.message_success, name="process_op"),
    path("subscribers", views.SubscriberList.as_view(), name="subscribers"),
    path(
        "js/bootstrap-static-base.js",
        views.bootstrap_static_base,
        name="bootstrap_static_base",
    ),
    path(
        "login/",
        ratelimit(group="bruteforce", rate="20/5m")(log_sec(
            views.CustomLoginView.as_view(
                authentication_form=clubuse_ui.forms.AuthFormCheckStatus,
                template_name="login.html",
            ),
            message=lambda r: "Login attempt: `%s`" % r.POST["username"]
            if r.POST else None,
        )),
        name="login",
    ),
    path(
        "logout",
        log_sec(auth_views.LogoutView.as_view(), message="Logout"),
        name="logout",
    ),
    # path("rpo/api/rest/schema/", get_schema_view(title="RPO BC API")),
    url(r"^api/register$", views.register_api),
    url(r"^api/optin_request$", views.get_optin),
    url(r"^api/toggle_state$", views.cambia_stato),
    url(r"^api/user_info$", views.get_user_info),
    url(r"^api/send_trx_optin$", views.send_trx_optin),
    url(r"^api/send_trx_state$", views.send_trx_state),
]
