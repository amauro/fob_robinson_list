import logging

from django.conf import settings
from django.shortcuts import render
from django.utils.deprecation import MiddlewareMixin


class HandleExceptionMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        if not settings.DEBUG and isinstance(exception, Exception):
            logging.exception("Errore")
            return render(request, "500.html", status=500)
