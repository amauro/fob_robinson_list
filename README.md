## Feature

Repository per l'implementazione del progetto Registro delle Opposizioni tramite Blockchain.

![](http://www.fub.it/sites/default/files/FUB_logo_Sito.png)

# Titolo del progetto

Questo è il repository principale del progetto Robinson List Blockchain
Il metodo di esecuzione suggerito è l'uso di docker-compose (per testing, sviluppo e deploy).

## Tabella dei Contenuti

[TOC]

## Sviluppo

### Prerequisiti e configurazione


È necessario installare docker e docker-compose.



### Creazione dell'ambiente di sviluppo, configurazione e building


```
docker-compose build
docker-compose up
./init.sh
```

è sufficiente per lanciare il progetto. La prima esecuzione potrà richiedere molto tempo, per via delle dipendenze da scaricare e dei container di cui fare il build.

Questo comando tuttavia porterà ad un'istanza priva di dati, e su cui è ancora necessario fare il setup del database di test ed altre configurazioni presenti nel file init.sh.

N.B Prima di lanciare lo script init.sh, attendere che il processo relativo al nodo algorand abbia completato il proprio avvio.

## Deploy/Installazione

### Prerequisiti e configurazione

_Inserire qui un semplice elenco dei software/librerie/linguaggi/compilatori/o.s. necessari per il rilascio o l'istallazione del software_

### Procedura

_Descrivere qui la/le procedure per installare il software negli ambienti di:_

- _test_
- _preproduzione_
- _produzione_

## Test

### Prerequisiti e configurazione

_Inserire qui un semplice elenco puntato degli eventuali software/librerie/linguaggi/compilatori/o.s. necessari per l'esecuzione dei test_

### Procedure di Test

_Descrivere qui la/le procedure per eseguire i test sul software per assicurarne la qualità e la conformità con le specifiche funzionali. I test minimi da eseguire sono:_

- _test di unità per ogni unità funzionale del sw_
- _test di integrazione tra le unità funzionali di cui sopra_

## Qualità del Codice

_Descrivere qui la/le procedure per verificare la qualità codice, tramite ad esempio Sonar, pylint, tool per verifica copertura dei test etc._

## Come contribuire

### Git branching model

Il modello utilizzato è quello in cui c'è un repository centrale, e ogni sviluppatore crea il suo fork.

La branch di sviluppo è `master`. Eventuali branch di release vanno chiamate ad esempio `release-1.0.0`.

I nomi di versione devono seguire [semantic versioning 2](https://semver.org/spec/v2.0.0.html).

Ai tag di versione va dato il prefisso `v` ad esempio `v1.0.0`

### Formattazione del codice

È *fortemente* raccomandato l'uso di questi tool

 * [black](https://github.com/psf/black) per formattare il codice python in modo consistente
 * [isort](https://pycqa.github.io/isort/) per ordinare gli import python per "importanza" (prima librerie builtin, poi terze parti, poi prime
     parti)
 * [standard](https://standardjs.com/) per formattare il codice javascript

Si consiglia di integrarli nel proprio editor/IDE, abilitando l'autoformattazione del codice ad ogni
salvataggio.

## Link

_Inserire qui informazioni come:_

- Homepage Progetto: http://algonode.fub.it/
- Repository: https://git.fub.it/rpo-blockchain/robinson-list
- Repository Documentale: https://drive.google.com/drive/u/1/folders/0AFHWgGIsWahRUk9PVA
- Repository per concorso FoB: https://gitlab.com/amauro/fob_robinson_list

## Licenza

_Inserire la licenza sotto la quale si intende rilasciare il sw. Esempio: "I codici di questo progetto sono rilasciati sotto la licenza GNU GPL.". Se necessario creare il file LICENSE con il contenuto della licenza_

