usage() {
    echo "$0 [options]"
    echo "Init RPO project"
    echo "Options:"
    echo " -p     Use only in production to call systemctl"
}

production=0 #by default test
while getopts ':p:' opt; do
    case $opt in
    p)
      production=1
      ;;
    h)
      usage
      exit 0
      ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        usage >&2
        exit 2
        ;;
    esac
done

#COPY ALGOD AND KMD TOKEN PARAMETERS PRIMARY
echo 'algod_token' > node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Primary/algod.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'algod_address' >> node_conf
echo 'http://algorand-node:8080' | tr '\n' ' ' >> node_conf #bindata in startup.sh
echo '' >> node_conf #just for new line
echo 'kmd_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Primary/kmd-v0.5/kmd.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'kmd_address' >> node_conf
echo 'http://algorand-node:7833' | tr '\n' ' ' >> node_conf #bindata in startup.sh
echo '' >> node_conf #just for new line

#COPY ALGOD AND KMD TOKEN PARAMETERS NODE 1
echo 'algod_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node1/algod.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'algod_address' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node1/algod.net | tr '\n' ' ' >> node_conf
echo 'kmd_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node1/kmd-v0.5/kmd.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'kmd_address' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node1/kmd-v0.5/kmd.net | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line

#COPY ALGOD AND KMD TOKEN PARAMETERS NODE2
echo 'algod_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node2/algod.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'algod_address' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node2/algod.net | tr '\n' ' ' >> node_conf
echo 'kmd_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node2/kmd-v0.5/kmd.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'kmd_address' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node2/kmd-v0.5/kmd.net | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line

#COPY ALGOD AND KMD TOKEN PARAMETERS NODE3
echo 'algod_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node3/algod.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'algod_address' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node3/algod.net | tr '\n' ' ' >> node_conf
echo 'kmd_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node3/kmd-v0.5/kmd.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'kmd_address' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node3/kmd-v0.5/kmd.net | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line

#COPY ALGOD AND KMD TOKEN PARAMETERS NODE 4
echo 'algod_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node4/algod.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'algod_address' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node4/algod.net | tr '\n' ' ' >> node_conf
echo 'kmd_token' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node4/kmd-v0.5/kmd.token | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line
echo 'kmd_address' >> node_conf
docker-compose exec algorand-node cat /blockchain/cu_net/Node4/kmd-v0.5/kmd.net | tr '\n' ' ' >> node_conf
echo '' >> node_conf #just for new line

#SET DEFAULT NODE
echo '0' >> node_conf #just for new line

#INIT DJANGO APP AND DB
docker-compose exec -T django-web python manage.py migrate
docker-compose exec django-web python manage.py collectstatic --no-input --clear
docker-compose exec -T django-web python manage.py create_user_default
#LOAD TEST DATA RPO
cat ./data/testdb.sql | docker-compose exec -T django-db psql -U clubuse

#SIMULATE WALLET WITH ALGOS
docker-compose exec -T django-web python manage.py init_algo_wallet

#CREATE ASSETS OPTIN-OPTOUT
docker-compose exec -T django-web python manage.py create_asset

#COMPILE AND DEPLOY SC, SEND ALL OPT_IN TOKEN TO SC AND SOME OPT_OUT
docker-compose exec -T django-web python manage.py create_sc;
docker cp opt_in_out_sc.teal "$(docker-compose ps -q algorand-node)":/blockchain/opt_in_out_sc.teal
docker-compose exec algorand-node ./goal clerk compile /blockchain/opt_in_out_sc.teal -o /opt_in_out_sc.lsig;
docker-compose exec algorand-node cat /opt_in_out_sc.lsig > opt_in_out_sc.lsig;
docker-compose exec -T django-web python manage.py deploy_smartcontract;

#FINALLY RESTART APP
if [ "$production" = "0" ] ; then
echo 'We are in test'
docker-compose restart;
elif [ "$production" = "1" ] ; then
echo 'We are in production'
systemctl restart algorandnode
fi