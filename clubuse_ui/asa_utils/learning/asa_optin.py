from algosdk import account, mnemonic, algod, transaction, encoding
import json

def wait_for_confirmation( algod_client, txid ):
    while True:
        txinfo = algod_client.pending_transaction_info(txid)
        if txinfo.get('round') and txinfo.get('round') > 0:
            print("Transaction {} confirmed in round {}.".format(txid, txinfo.get('round')))
            return txinfo
        else:
            print("Waiting for confirmation...")
            algod_client.status_after_block(algod_client.status().get('lastRound') +1)

algod_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
algod_address = "http://127.0.0.1:4001"

# create a kmd and algod client
acl = algod.AlgodClient(algod_token, algod_address)

private_key = mnemonic.to_private_key("grace hybrid jazz injury combine figure hero suspect miracle suffer goat response harbor finish mercy veteran fold involve negative wool enhance risk mansion about solve")
addr = account.address_from_private_key(private_key)
print("Account successfully imported: ", addr)

# get suggested parameters
params = acl.suggested_params()
gen = params["genesisID"]
gh = params["genesishashb64"]
last_round = params["lastRound"]
fee = params["fee"]

# asset id
asset_id = 183033

account_info = acl.account_info(addr)
holding = None
if 'assets' in account_info:
    holding = account_info['assets'].get(str(asset_id))

if not holding:
    # Get latest network parameters
    data = {
        "sender": addr,
        "fee": fee,
        "first": last_round,
        "last": last_round+1000,
        "gh": gh,
        "receiver": addr,
        "amt": 0,
        "index": asset_id,
        "flat_fee": True
    }

    print("Asset Option In")
    # Use the AssetTransferTxn class to transfer assets
    txn = transaction.AssetTransferTxn(**data)
    stxn = txn.sign(private_key)
    txid = acl.send_transaction(stxn, headers={'content-type': 'application/x-binary'})
    print(txid)
    # Wait for the transaction to be confirmed
    wait_for_confirmation(acl, txid)
    # Now check the asset holding for that account. 
    # This should now show a holding with a balance of 0.
    account_info = acl.account_info(addr)
    print(json.dumps(account_info['assets'][str(asset_id)], indent=4))