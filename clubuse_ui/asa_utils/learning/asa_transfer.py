from algosdk import account, mnemonic, algod, transaction, encoding
import json

def wait_for_confirmation( algod_client, txid ):
    while True:
        txinfo = algod_client.pending_transaction_info(txid)
        if txinfo.get('round') and txinfo.get('round') > 0:
            print("Transaction {} confirmed in round {}.".format(txid, txinfo.get('round')))
            return txinfo
        else:
            print("Waiting for confirmation...")
            algod_client.status_after_block(algod_client.status().get('lastRound') +1)

algod_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
algod_address = "http://127.0.0.1:4001"

# create a kmd and algod client
acl = algod.AlgodClient(algod_token, algod_address)

private_key_1 = mnemonic.to_private_key("rural blind dad curtain select find genre rescue car icon fat adjust tiger fuel decide diesel learn unknown move board bamboo grass song absent office")
addr_1 = account.address_from_private_key(private_key_1)
print("Account successfully imported: ", addr_1)

private_key_2 = mnemonic.to_private_key("grace hybrid jazz injury combine figure hero suspect miracle suffer goat response harbor finish mercy veteran fold involve negative wool enhance risk mansion about solve")
addr_2 = account.address_from_private_key(private_key_2)
print("Account successfully imported: ", addr_2)

# get suggested parameters
params = acl.suggested_params()
gen = params["genesisID"]
gh = params["genesishashb64"]
last_round = params["lastRound"]
fee = params["fee"]

# create a transaction
sender = addr_1
recipient = addr_2

# asset id
asset_id = 183033

account_info = acl.account_info(recipient)

# Get latest network parameters
data = {
    "sender": sender,
    "fee": fee,
    "first": last_round,
    "last": last_round+1000,
    "gh": gh,
    "receiver": recipient,
    "amt": 0,
    "index": asset_id,
    "flat_fee": True
}

print("Asset transfer")
# Use the AssetTransferTxn class to transfer assets
txn = transaction.AssetTransferTxn(**data)
stxn = txn.sign(private_key_1)
txid = acl.send_transaction(stxn, headers={'content-type': 'application/x-binary'})
print(txid)
# Wait for the transaction to be confirmed
wait_for_confirmation(acl, txid)
# Now check the asset holding for that account. 
# This should now show a holding with a balance of 0.
account_info = acl.account_info(sender)
print(json.dumps(account_info['assets'][str(asset_id)], indent=4))