from algosdk import account, mnemonic, algod, transaction, encoding

def wait_for_confirmation( algod_client, txid ):
    while True:
        txinfo = algod_client.pending_transaction_info(txid)
        if txinfo.get('round') and txinfo.get('round') > 0:
            print("Transaction {} confirmed in round {}.".format(txid, txinfo.get('round')))
            return txinfo
        else:
            print("Waiting for confirmation...")
            algod_client.status_after_block(algod_client.status().get('lastRound') +1)

algod_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
algod_address = "http://127.0.0.1:4001"

# create a kmd and algod client
acl = algod.AlgodClient(algod_token, algod_address)

private_key = mnemonic.to_private_key("rural blind dad curtain select find genre rescue car icon fat adjust tiger fuel decide diesel learn unknown move board bamboo grass song absent office")
addr = account.address_from_private_key(private_key)
print("Account successfully imported: ", addr)

# get suggested parameters
params = acl.suggested_params()
gen = params["genesisID"]
gh = params["genesishashb64"]
last_round = params["lastRound"]
fee = params["fee"]

# create a transaction
sender = addr
recipient = "4CXXFP3SJJW63HGEQD4OPSDPPIYDW7BXCVP4DQZG7T33Z3BXTOA4UMEDM4"
amount = 10000

# asset id
asset_id = 182800
# Destroy Asset
data = {
    "sender": addr,
    "fee": fee,
    "first": last_round,
    "last": last_round+1000,
    "gh": gh,
    "index": asset_id,
    "flat_fee": True,
    "strict_empty_address_check": False
}

print("Destroying Asset")
# Construct Asset Creation transaction
txn = transaction.AssetConfigTxn(**data)

# Sign with secret key of creator
stxn = txn.sign(private_key)

# Send the transaction to the network and retrieve the txid.
txid = acl.send_transaction(stxn, headers={'content-type': 'application/x-binary'})
print(txid)

# Wait for the transaction to be confirmed
wait_for_confirmation(acl, txid)

# This should raise an exception since the asset was deleted.
try:
    asset_info = acl.asset_info(asset_id)
except Exception as e:
    print("DESTROYED")
    print(e)