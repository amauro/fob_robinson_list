from algosdk import account, mnemonic, algod, transaction, encoding


def wait_for_confirmation(algod_client, txid):
    while True:
        txinfo = algod_client.pending_transaction_info(txid)
        if txinfo.get("round") and txinfo.get("round") > 0:
            print(
                "Transaction {} confirmed in round {}.".format(
                    txid, txinfo.get("round")
                )
            )
            return txinfo
        else:
            print("Waiting for confirmation...")
            algod_client.status_after_block(algod_client.status().get("lastRound") + 1)


algod_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
algod_address = "http://127.0.0.1:4001"

# create a kmd and algod client
acl = algod.AlgodClient(algod_token, algod_address)

private_key = mnemonic.to_private_key(
    "rural blind dad curtain select find genre rescue car icon fat adjust tiger fuel decide diesel learn unknown move board bamboo grass song absent office"
)
addr = account.address_from_private_key(private_key)
print("Account successfully imported: ", addr)

# get suggested parameters
params = acl.suggested_params()
gen = params["genesisID"]
gh = params["genesishashb64"]
last_round = params["lastRound"]
fee = params["fee"]

# create a transaction
sender = addr
recipient = "4CXXFP3SJJW63HGEQD4OPSDPPIYDW7BXCVP4DQZG7T33Z3BXTOA4UMEDM4"
amount = 10000

# Configure fields for creating the asset.
data = {
    "sender": addr,
    "fee": fee,
    "first": last_round,
    "last": last_round + 1000,
    "gh": gh,
    "total": 1,
    "decimals": 0,
    "default_frozen": False,
    "unit_name": "optin",
    "asset_name": "optin",
    "manager": addr,
    "reserve": addr,
    "freeze": addr,
    "clawback": addr,
    "url": "https://path/to/my/asset/details",
    "flat_fee": True,
}

# Construct Asset Creation transaction
txn = transaction.AssetConfigTxn(**data)
stxn = txn.sign(private_key)

print("Asset Creation")

# Send the transaction to the network and retrieve the txid.
txid = acl.send_transaction(stxn, headers={"content-type": "application/x-binary"})
# Retrieve the asset ID of the newly created asset by first
# ensuring that the creation transaction was confirmed,
# then pulling account info of the creator and grabbing the
# asset with the max asset ID.
# Wait for the transaction to be confirmed
txinfo = wait_for_confirmation(acl, txid)
print(txinfo.keys())
print(txinfo)
asset_id = txinfo["txresults"]["createdasset"]
account_info = acl.account_info(addr)
