from algosdk import account, mnemonic, algod, transaction, encoding

algod_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
algod_address = "http://127.0.0.1:4001"

# create a kmd and algod client
acl = algod.AlgodClient(algod_token, algod_address)

private_key = mnemonic.to_private_key("rural blind dad curtain select find genre rescue car icon fat adjust tiger fuel decide diesel learn unknown move board bamboo grass song absent office")
addr = account.address_from_private_key(private_key)
print("Account successfully imported: ", addr)

# get suggested parameters
params = acl.suggested_params()
gen = params["genesisID"]
gh = params["genesishashb64"]
last_round = params["lastRound"]
fee = params["fee"]

# create a transaction
sender = addr
recipient = "4CXXFP3SJJW63HGEQD4OPSDPPIYDW7BXCVP4DQZG7T33Z3BXTOA4UMEDM4"
amount = 10000
txn = transaction.PaymentTxn(sender, fee, last_round, last_round+100, gh, recipient, amount)

sig = txn.sign(private_key)

# print encoded transaction
print(sig)
print(encoding.msgpack_encode(txn))

# send the transaction
transaction_id = acl.send_raw_transaction(encoding.msgpack_encode(sig))
print("\nTransaction was sent!")
print("Transaction ID: " + transaction_id + "\n")