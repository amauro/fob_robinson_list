from algosdk import account, mnemonic, encoding
from algosdk.v2client import algod
from algosdk import algod as algodv1
from algosdk.future import transaction
import json, sys
from clubuse_ui.asa_utils.util import wait_for_confirmation
import binascii

# from sc import make_sc


def send_money(acl, addr, private_key, receiver, amount, note=None):
    txn = make_pay_transaction(acl, addr, receiver, amount, note=note)
    signed_txn = txn.sign(private_key)
    txid = signed_txn.transaction.get_txid()
    # print("Signed transaction with txID: {}".format(txid))

    acl.send_transaction(signed_txn)

    # wait for confirmation
    wait_for_confirmation(acl, txid)


def make_pay_transaction(acl, addr, receiver, amount, note=None):
    # get suggested parameters CHANGETOV1
    try:
        params = acl.suggested_params()
    except:
        algod_token = acl.algod_token
        algod_address = acl.algod_address
        acl = algodv1.AlgodClient(algod_token, algod_address)
        params = acl.suggested_params()

    if type(params) == transaction.SuggestedParams:
        sp = params
    else:
        sp = transaction.SuggestedParams(
            params["fee"],
            params["lastRound"],
            params["lastRound"] + 100,
            params["genesishashb64"],
            params["genesisID"],
            flat_fee=True,
        )

    txn = transaction.PaymentTxn(addr, sp, receiver, amount, note)
    return txn


def create_asa(acl,
               addr,
               private_key,
               asset_name,
               total_asset=1000):  # 1 bilion
    # get suggested parameters
    txn = make_create_asa(acl, addr, total_asset, asset_name)
    stxn = txn.sign(private_key)
    # Send the transaction to the network and retrieve the txid.
    txid = acl.send_transaction(
        stxn, headers={"content-type": "application/x-binary"})
    # Retrieve the asset ID of the newly created asset by first
    # ensuring that the creation transaction was confirmed,
    # then pulling account info of the creator and grabbing the
    # asset with the max asset ID.
    # Wait for the transaction to be confirmed
    txinfo = wait_for_confirmation(acl, txid)
    # print(txinfo.keys())
    # print(txinfo)
    return txinfo["txresults"]["createdasset"]


def make_create_asa(acl, addr, total_asset, asset_name):
    # get suggested parameters
    params = acl.suggested_params()
    if type(params) == transaction.SuggestedParams:
        sp = params
    else:
        sp = transaction.SuggestedParams(
            params["fee"],
            params["lastRound"],
            params["lastRound"] + 100,
            params["genesishashb64"],
            params["genesisID"],
            flat_fee=True,
        )

    # Construct Asset Creation transaction
    txn = transaction.AssetConfigTxn(
        addr,
        sp,
        total=total_asset,
        manager=addr,
        reserve=addr,
        freeze=addr,
        clawback=addr,
        url="https://path/to/my/asset/details",
        unit_name=asset_name,
        asset_name=asset_name,
        default_frozen=False,
    )
    return txn


def optin_asa(acl,
              addr,
              private_key,
              asset_id,
              send=True,
              skip_unusefull=False):
    # CHANGETOV1
    algod_token = acl.algod_token
    algod_address = acl.algod_address
    acl = algodv1.AlgodClient(algod_token, algod_address)

    # get suggested parameters
    txn = make_optin_asa(acl, addr, asset_id)
    stxn = txn.sign(private_key)
    txid = acl.send_transaction(
        stxn, headers={"content-type": "application/x-binary"})
    print(txid)
    # Wait for the transaction to be confirmed
    if not skip_unusefull:
        wait_for_confirmation(acl, txid)


def optin_asa_sc(acl, addr, program, asset_id):

    # get suggested parameters
    txn = make_optin_asa(acl, addr, asset_id)

    # write to file
    lsig = transaction.LogicSig(program)
    stxn = transaction.LogicSigTransaction(txn, lsig)
    txid = acl.send_transaction(
        stxn, headers={"content-type": "application/x-binary"})
    print(txid)
    # Wait for the transaction to be confirmed
    wait_for_confirmation(acl, txid)
    # Now check the asset holding for that account.
    # This should now show a holding with a balance of 0.
    account_info = acl.account_info(addr)


def make_optin_asa(acl, addr, asset_id):
    # get suggested parameters CHANGETOV1
    try:
        params = acl.suggested_params()
    except:
        algod_token = acl.algod_token
        algod_address = acl.algod_address
        acl = algodv1.AlgodClient(algod_token, algod_address)
        params = acl.suggested_params()

    if type(params) == transaction.SuggestedParams:
        sp = params
    else:
        sp = transaction.SuggestedParams(
            params["fee"],
            params["lastRound"],
            params["lastRound"] + 100,
            params["genesishashb64"],
            params["genesisID"],
            flat_fee=True,
        )
    # print("SC-Asset Option In")
    # Use the AssetTransferTxn class to transfer assets
    txn = transaction.AssetTransferTxn(addr, sp, addr, 0, asset_id)
    return txn
    # print(json.dumps(account_info["assets"][str(asset_id)], indent=4))


def transfer_asa(
    acl,
    sender,
    recipient,
    private_key,
    amount,
    asset_id,
    logic=[False, None],
    skip_unusefull=False,
):
    # get suggested parameters
    txn = make_transfer_asa(acl, recipient, sender, amount, asset_id)

    if not logic[0]:
        stxn = txn.sign(private_key)

        # CHANGETOV1
        try:
            txid = acl.send_transaction(
                stxn, headers={"content-type": "application/x-binary"})
        except:
            algod_token = acl.algod_token
            algod_address = acl.algod_address
            acl = algodv1.AlgodClient(algod_token, algod_address)
            txid = acl.send_transaction(
                stxn, headers={"content-type": "application/x-binary"})
        print(txid, "hereee")
        # Wait for the transaction to be confirmed
        if not skip_unusefull:
            wait_for_confirmation(acl, txid)
    else:
        lsig = transaction.LogicSig(logic[1])
        lstx = transaction.LogicSigTransaction(txn, lsig)
        txid = acl.send_transaction(lstx)

    # Now check the asset holding for that account.
    # This should now show a holding with a balance of 0.
    if not skip_unusefull:
        account_info = acl.account_info(sender)
        print(json.dumps(account_info["assets"], indent=4))

    return txid


def make_transfer_asa(acl, recipient, sender, amount, asset_id):
    # CHANGETOV1
    try:
        params = acl.suggested_params()
    except:
        algod_token = acl.algod_token
        algod_address = acl.algod_address
        acl = algodv1.AlgodClient(algod_token, algod_address)
        params = acl.suggested_params()
    # print("SC-Asset Option In")
    # Use the AssetTransferTxn class to transfer assets
    if type(params) == transaction.SuggestedParams:
        sp = params
    else:
        sp = transaction.SuggestedParams(
            params["fee"],
            params["lastRound"],
            params["lastRound"] + 100,
            params["genesishashb64"],
            params["genesisID"],
            flat_fee=True,
        )
    txn = transaction.AssetTransferTxn(sender, sp, recipient, amount, asset_id)
    return txn


def delete_asa(acl, addr, private_key, asset_id):
    # get suggested parameters
    params = acl.suggested_params()
    txn = transaction.AssetConfigTxn(addr,
                                     params,
                                     index=asset_id,
                                     strict_empty_address_check=False)

    # Sign with secret key of creator
    stxn = txn.sign(private_key)

    # Send the transaction to the network and retrieve the txid.
    txid = acl.send_transaction(
        stxn, headers={"content-type": "application/x-binary"})
    print(txid)

    # Wait for the transaction to be confirmed
    wait_for_confirmation(acl, txid)


def generate_optin_group(
    acl,
    account_user,
    sk_user,
    account_sc,
    logic,
    from_asset_id,
    to_asset_id,
    request=None,
):
    # create transaction1
    txn1 = make_transfer_asa(acl, account_sc, account_user, 1, from_asset_id)

    # create transaction2
    txn2 = make_transfer_asa(acl, account_user, account_sc, 1, to_asset_id)

    # get group id and assign it to transactions
    gid = transaction.calculate_group_id([txn1, txn2])
    txn1.group = gid
    txn2.group = gid

    # sign transaction1
    stxn1 = txn1.sign(sk_user)

    # sign transaction2
    lsig = transaction.LogicSig(logic)
    stxn2 = transaction.LogicSigTransaction(txn2, lsig)

    signedGroup = []
    signedGroup.append(stxn1)
    signedGroup.append(stxn2)

    # send them over network
    sent = acl.send_transactions(signedGroup)
    # print txid
    print(sent)

    # wait for confirmation
    wait_for_confirmation(acl, sent, request)


def generate_optin_group_trx(
    acl,
    account_user,
    account_sc,
    logic,
    from_asset_id,
    to_asset_id,
):
    # create transaction1
    txn1 = make_transfer_asa(acl, account_sc, account_user, 1, from_asset_id)

    # create transaction2
    txn2 = make_transfer_asa(acl, account_user, account_sc, 1, to_asset_id)

    # get group id and assign it to transactions
    gid = transaction.calculate_group_id([txn1, txn2])
    txn1.group = gid
    txn2.group = gid

    # sign transaction1
    stxn1 = txn1

    # sign transaction2
    lsig = transaction.LogicSig(logic)
    stxn2 = transaction.LogicSigTransaction(txn2, lsig)

    signedGroup = []
    signedGroup.append(stxn1)
    signedGroup.append(stxn2)

    return signedGroup


def generate_asas(acl, addr, private_key):
    txn1 = make_create_asa(acl, addr, 1000, "OPTOUT")
    txn2 = make_create_asa(acl, addr, 1000, "OPTIN")

    # get group id and assign it to transactions
    gid = transaction.calculate_group_id([txn1, txn2])
    txn1.group = gid
    txn2.group = gid

    # sign transaction1
    stxn1 = txn1.sign(private_key)

    # sign transaction2
    stxn2 = txn2.sign(private_key)

    signedGroup = []
    signedGroup.append(stxn1)
    signedGroup.append(stxn2)

    # send them over network
    sent = acl.send_transactions(signedGroup)

    # wait for confirmation
    wait_for_confirmation(acl, sent)
    account_info = acl.account_info(addr)
    asset_ids = list(account_info["assets"].keys())

    return int(asset_ids[0]), int(asset_ids[1])


def setup_user_group(acl, fub_addr, fub_private, addr, private_key, asset_list,
                     opt_out_asset_id):
    # create transaction1
    txn1 = make_pay_transaction(acl, fub_addr, addr, 1000000)

    # create optin transactions
    txn_optin = []
    for asset_id in asset_list:
        txn_optin.append(make_optin_asa(acl, addr, asset_id))

    # create transfer tx
    txn2 = make_transfer_asa(acl, addr, fub_addr, 1, opt_out_asset_id)

    # get group id and assign it to transactions
    txns = [txn1]
    for tx in txn_optin:
        txns.append(tx)
    txns.append(txn2)

    gid = transaction.calculate_group_id(txns)

    txn1.group = gid
    for tx in txn_optin:
        tx.group = gid
    txn2.group = gid

    # sign transaction1
    stxn1 = txn1.sign(fub_private)

    # sign group transaction optin
    stxn_optin = []
    for tx in txn_optin:
        stxn_optin.append(tx.sign(private_key))

    # sign transaction2
    stxn2 = txn2.sign(fub_private)

    signedGroup = []
    signedGroup.append(stxn1)
    for stx in stxn_optin:
        signedGroup.append(stx)
    signedGroup.append(stxn2)

    # send them over network
    sent = acl.send_transactions(signedGroup)

    # wait for confirmation
    wait_for_confirmation(acl, sent)
