from sc import make
import sys

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("You have passed {0} parameters!".format(len(sys.argv)))
        print("Pass 3 parameters in the following order: <SC-ADDR>, <OPTOUT-tokenID>, <OPTIN-tokenID>")
    else:
        try: 
            print(make(sys.argv[1], int(sys.argv[2]), int(sys.argv[3])))
        except ValueError as err:
            print("ValueError: {0}".format(err))
        except Exception as err:
            raise
