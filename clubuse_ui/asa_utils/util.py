from algosdk import account, mnemonic, transaction, encoding
from algosdk.v2client import algod
import json, sys
from django.contrib import messages


def wait_for_confirmation(algod_client, txid, request=None):
    while True:
        txinfo = algod_client.pending_transaction_info(txid)
        if (txinfo.get("confirmed-round") and txinfo.get("confirmed-round") > 0) or (
            txinfo.get("round") and txinfo.get("round") > 0
        ):
            print(
                "Transaction {} confirmed in round {}.".format(
                    txid, txinfo.get("confirmed-round")
                )
            )
            if request:
                messages.success(
                    request,
                    "Transaction {} confirmed in round {}.".format(
                        txid, txinfo.get("confirmed-round")
                    ),
                )
            return txinfo
        else:
            print("Waiting for confirmation...")
            if request:
                messages.success(request, "Waiting for confirmation...")
            # print(algod_client.status())
            if algod_client.status().get("last-round"):
                last_round = algod_client.status().get("last-round") + 1
                algod_client.status_after_block(last_round)


def wait_for_money(algod_client, addr):
    print("Wait {0} receiving the money ...".format(addr))
    while True:
        account_info = algod_client.account_info(addr)
        if int(account_info.get("amount")) > 0:
            print("Account balence now is: {0}!".format(account_info.get("amount")))
            return
