from pyteal import *
from clubuse_ui.algosdk_util import params

""" SC contract for managing OPT-IN/OPT-OUT"""


def atomic_transactions(opt_out_asset_id, opt_in_asset_id):

    type1 = Gtxn.type_enum(0) == Int(4)
    type2 = Gtxn.type_enum(1) == Int(4)
    type_condition = And(Global.group_size() == Int(2), type1, type2)

    out_to_in_first_tx = And(
        Gtxn.xfer_asset(0) == Int(opt_out_asset_id), Gtxn.asset_amount(0) == Int(1)
    )
    out_to_in_sec_tx = And(
        Gtxn.xfer_asset(1) == Int(opt_in_asset_id), Gtxn.asset_amount(1) == Int(1)
    )
    opt_in = And(out_to_in_first_tx, out_to_in_sec_tx)

    in_to_out_first_tx = And(
        Gtxn.xfer_asset(0) == Int(opt_in_asset_id), Gtxn.asset_amount(0) == Int(1)
    )
    in_to_out_sec_tx = And(
        Gtxn.xfer_asset(1) == Int(opt_out_asset_id), Gtxn.asset_amount(1) == Int(1)
    )
    opt_out = And(in_to_out_first_tx, in_to_out_sec_tx)

    OR_condition = Or(opt_in, opt_out)

    final_asset = And(type_condition, OR_condition)
    return final_asset


def make(sc_addr, opt_out, opt_in):
    res = If(
        Global.group_size() == Int(1),
        one_tx(sc_addr, opt_out, opt_in),
        atomic_transactions(opt_out, opt_in),
    )
    final = And(res, Txn.fee() >= Int(1000))
    return final.teal()


def one_tx(sc_addr, opt_out, opt_in):
    type_and_zero_amount = And(
        Txn.type_enum() == Int(4),
        Txn.asset_sender() == Global.zero_address(),
        Txn.asset_amount() == Int(0),
    )
    which_asset_id = Or(
        Txn.xfer_asset() == Int(opt_out), Txn.xfer_asset() == Int(opt_in)
    )

    noose_tx = And(
        Txn.asset_receiver() == Txn.sender(),
        Txn.asset_close_to() == Global.zero_address(),
    )

    one_tx_ver = And(noose_tx, which_asset_id, type_and_zero_amount)
    # return Or(pay_tx, fstf)
    return one_tx_ver


# print(make("L6XU3EEVKLATGPZ3PMGZY6S6TZR7WHKDBVYVL4RXRPCVIXJWZRWB5JR4GU", 184270,184271))

# print(make_opt_in(184270))
