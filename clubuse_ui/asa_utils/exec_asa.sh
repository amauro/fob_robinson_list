#TODO need to update smart contract attestator address, just created
#TOD copy token id OPTIN OPTOUT
# sudo cp opt_in_out_sc.teal /var/lib/docker/volumes/asa_rpo_test_node_rpo_data/_data
helpFunction()
{
   echo ""
   echo "Usage: $0 -a sc-addr -o optout-assetid -i optin-assetid"
   echo -e "\t-a SC address"
   echo -e "\t-o Optout assetID"
   echo -e "\t-i Optin assetID"
   exit 1 # Exit script after printing help
}

while getopts "a:o:i:" opt
do
   case "$opt" in
      a ) scaddr="$OPTARG" ;;
      o ) optoutassetid="$OPTARG" ;;
      i ) optinassetid="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$scaddr" ] || [ -z "$optoutassetid" ] || [ -z "$optinassetid" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

python3 make_sc.py "$scaddr" "$optoutassetid" "$optinassetid" > opt_in_out_sc.teal
docker cp opt_in_out_sc.teal sandbox:/opt/algorand/opt_in_out_sc.teal

docker exec sandbox ./goal clerk compile opt_in_out_sc.teal -o /opt_in_out_sc.lsig;
docker cp sandbox:/opt/algorand/opt_in_out_sc.lsig .
python3 asa.py