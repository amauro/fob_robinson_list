from algosdk import account, mnemonic, algod, transaction, encoding
import json, sys
from util import wait_for_confirmation, wait_for_money
from tx_util import *
import subprocess
import time
from sc import make
import binascii

# from sc import make_sc

if __name__ == "__main__":
    algod_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    algod_address = "http://127.0.0.1:4001"
    headers = {}


    if len(sys.argv) < 2:
        print("You have passed {0} parameters!".format(len(sys.argv)))
        print("Pass the number of users you want to test")
        exit(1)
    
    print(">>>>>>> SETUP OF THE SYSTEM <<<<<<<\n\n\n")

    # create algod client
    acl = algod.AlgodClient(algod_token, algod_address, headers)

    users = []
    fub_private_key, fub_addr = account.generate_account()    
    print("FUB address: {0}".format(fub_addr))
    print("Secret mnemonic: {0}\n\n".format(mnemonic.from_private_key(fub_private_key)))
    wait_for_money(acl, fub_addr)


    # fub_private_key = mnemonic.to_private_key(
    #     "typical still random dance blouse auto rather salmon misery brown grain journey cake mosquito gather that ghost seminar giggle expect purchase detail shield able palm"
    # )
    # fub_addr = account.address_from_private_key(fub_private_key)
    # print("Account successfully imported: ", fub_addr)

    # user_private_key = mnemonic.to_private_key(
    #     "sorry pink aisle evolve comfort bacon make unaware giant able knock depart chaos canyon legal victory surprise left please defense aisle cotton swim abandon lecture"
    # )
    # user_addr = account.address_from_private_key(user_private_key)
    # print("Account successfully imported: ", user_addr)

    # account_info = acl.account_info(fub_addr)
    # print(json.dumps(account_info["assets"], indent=4))
    
    print(">>> Creating the ASAs ...")
    opt_out_asset_id, opt_in_asset_id = generate_asas(acl, fub_addr, fub_private_key)
    print(">>> opt_out_asset_id: {0}".format(opt_out_asset_id))
    print(">>> opt_in_asset_id: {0}\n\n".format(opt_in_asset_id))
    # If they already exist
    # opt_out_asset_id = 184270
    # opt_in_asset_id = 184271

    # Make smart contract 
    sc_text = make(fub_addr, opt_out_asset_id, opt_in_asset_id)
    sc_file_teal = open("opt_in_out_sc.teal", "w").write(sc_text)

    # Compile smart contract
    print(">>> Compiling SC ...")
    shellscript = subprocess.call(["./help_asa.sh"])
    print("... Done!\n")

    # retrieve and deploy smart contract
    program = open("opt_in_out_sc.lsig", "rb").read()
    sc_addr = transaction.LogicSig(program).address()  # CHI IL CREATORE DELLO SC

    print(">>> Sending money to the SCs ...\n\n")
    send_money(acl, fub_addr, fub_private_key, sc_addr, 1000000)

    # smart contract optin assets
    print(">>> OPTING-IN of the ASAs for SC ...\n\n")
    optin_asa_sc(acl, sc_addr, program, opt_in_asset_id)
    optin_asa_sc(acl, sc_addr, program, opt_out_asset_id)

    print(">>> Transfer Optin ASAs to the SC ...\n\n")
    transfer_asa(
        acl, fub_addr, sc_addr, fub_private_key, 1000, opt_in_asset_id
    )  # tranfer to the SC

    # optin assets
    print(">>>>>>> SETUP USERS <<<<<<<\n\n\n")
    number_users = int(sys.argv[1])
    print(">>> Creating {0} users\n\n".format(int(sys.argv[1])))

    for i in range(0, number_users):
        start_time = time.time()
        users.append(account.generate_account())
        print(">>> New user address: {0}\n\n".format(users[i][1]))

        user_addr = users[i][1]
        user_assets_needing = []
        account_info = acl.account_info(user_addr)
        for asset_id in (opt_in_asset_id, opt_out_asset_id):
            holding = None
            if "assets" in account_info:
                holding = account_info["assets"].get(str(asset_id))

            if not holding:
                user_assets_needing.append(asset_id)
        
        # TODO AUTHENTICATION user-address/user-identity

        print(">>> Setup User {0}!\n\n".format(users[i][1]))
        setup_user_group(acl, fub_addr, fub_private_key, user_addr, users[i][0], user_assets_needing, opt_out_asset_id)
        print("Time interval to Setup User {0}: {1}".format(users[i][1], time.time()-start_time))
    
    print(">>> User {0} is opting in...\n\n".format(users[0][1]))
    start_time = time.time()
    generate_optin_group(
        acl,
        users[0][1],
        users[0][0],
        sc_addr,
        program,
        opt_out_asset_id,
        opt_in_asset_id
    )
    print("Time interval to Optin User {0}: {1}".format(users[i][1], time.time()-start_time))

    print(">>> User {0} is opting in...\n\n".format(users[1][1]))
    start_time = time.time()
    generate_optin_group(
        acl,
        users[1][1],
        users[1][0],
        sc_addr,
        program,
        opt_out_asset_id,
        opt_in_asset_id
    )
    print("Time interval to optin User {0}: {1}".format(users[i][1], time.time()-start_time))

    print(">>> User {0} is optout out...\n\n".format(users[0][1]))
    start_time = time.time()    
    generate_optin_group(
        acl,
        users[0][1],
        users[0][0],
        sc_addr,
        program,
        opt_in_asset_id,
        opt_out_asset_id
    )
    print("Time interval to optout User {0}: {1}".format(users[i][1], time.time()-start_time))

    print(">>> User {0} is opting in...\n\n".format(users[0][1]))
    start_time = time.time()
    generate_optin_group(
        acl,
        users[0][1],
        users[0][0],
        sc_addr,
        program,
        opt_out_asset_id,
        opt_in_asset_id
    )
    print("Time interval to optin User {0}: {1}".format(users[i][1], time.time()-start_time))
    
    print(">>> User {0} is opting in...\n\n".format(users[2][1]))
    start_time = time.time()
    generate_optin_group(
        acl,
        users[2][1],
        users[2][0],
        sc_addr,
        program,
        opt_out_asset_id,
        opt_in_asset_id
    )
    print("Time interval to optin User {0}: {1}".format(users[i][1], time.time()-start_time))