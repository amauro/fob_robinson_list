from django.apps import AppConfig


class ClubuseUiConfig(AppConfig):
    name = 'clubuse_ui'
