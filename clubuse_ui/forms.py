import json
import logging

from django import forms
from django.apps import apps
from django.conf import settings
from django.contrib.auth.forms import (
    AuthenticationForm,
    PasswordResetForm,
    UsernameField,
)
from clubuse_ui.models import User
from django.contrib.auth.forms import UserCreationForm
from django.template import loader
from django.db import transaction
from django.db.models import Q
from django.core.validators import FileExtensionValidator
from django.forms import ValidationError, modelformset_factory
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.safestring import mark_safe
from bootstrap_italia_template.widgets import (
    BootstrapItaliaRadioWidget,
    BootstrapItaliaSelectWidget,
    BootstrapItaliaDateWidget,
)
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator


class AuthFormCheckStatus(AuthenticationForm):
    username = UsernameField(widget=forms.TextInput(attrs={"autofocus": False}))

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                "We are sorry but your account has been disabled.", code="inactive"
            )


class SignUpForm(UserCreationForm):
    mob_phone = forms.CharField(max_length=100, required=True, label="Mobile Phone")

    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields

    def save(self, commit=True):
        user = super().save(commit=False)
        user.mob_phone = self.cleaned_data["mob_phone"]
        if commit:
            user.save()
        return user
