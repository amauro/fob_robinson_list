from django import template
from django.db.models.query import QuerySet
import os, json
from clubuse_ui.models import User
import datetime

from clubuse_ui.algosdk_util import params
from algosdk import account, transaction, algod, encoding, mnemonic, kmd

from django.utils.translation import ugettext_lazy as _

register = template.Library()


@register.filter
def choicedisplay(obj, fieldname):
    choices = obj._meta.get_field(fieldname).choices
    if getattr(obj, fieldname):
        return dict(choices)[getattr(obj, fieldname)]
    else:
        return ""


@register.filter
def balance_disp(addr):
    acl = algod.AlgodClient(params.algod_token, params.algod_address)
    return acl.account_info(addr)["amount"]


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def filename(value):
    return os.path.basename(value.file.name)


@register.filter
def bool_disp(value):
    if value:
        return _("SI")
    else:
        return "NO"


@register.filter
def addstr(arg1, arg2):
    """concatenate arg1 & arg2"""
    return str(arg1) + str(arg2)


@register.filter
def verbose_field_name(instance, field_name):
    """
    Returns verbose_name for a field.
    """
    verbose_name = instance._meta.get_field(field_name).verbose_name
    verbose_name = "%s%s" % (verbose_name[0].upper(), verbose_name[1:])
    return verbose_name


@register.filter
def pretty_json(value):
    return json.dumps(value, indent=4)


@register.filter
def unix_to_datetime(value):
    if value:
        return datetime.datetime.fromtimestamp(int(value))
    else:
        return None
