# Generated by Django 2.2.10 on 2020-03-04 11:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clubuse_ui', '0003_auto_20200226_1033'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='handle',
        ),
    ]
