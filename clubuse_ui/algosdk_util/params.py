from os import environ

# change these after starting the node and kmd
# algod info is in the algod.net and algod.token files in the data directory
# kmd info is in the kmd.net and kmd.token files in the kmd directory in data
# change these after starting the node and kmd

kmd_token = ""
kmd_address = ""
algod_token = ""
algod_address = ""

kmd_token_primary = ""
kmd_address_primary = "http://algorand-node:7833"

algod_token_primary = ""
algod_address_primary = "http://algorand-node:8080"

opt_in_asset_id = 7
opt_out_asset_id = 8

kmd_token1 = ""
kmd_address1 = "http://algorand-node:7833"

algod_token1 = ""
algod_address1 = "http://algorand-node:8080"

kmd_token2 = ""
kmd_address2 = "http://algorand-node:7833"

algod_token2 = ""
algod_address2 = "http://algorand-node:8080"

kmd_token3 = ""
kmd_address3 = "http://algorand-node:7833"

algod_token3 = ""
algod_address3 = "http://algorand-node:8080"

kmd_token4 = ""
kmd_address4 = "http://algorand-node:7833"

algod_token4 = ""
algod_address4 = "http://algorand-node:8080"

# get tokens and addresses automatically, if data_dir_path is not empty
with open("node_conf", "r") as conf:
    content = [line.rstrip() for line in conf]
    # solo righe pari, prima intestazione
    algod_token_primary = content[1]
    algod_address_primary = content[3]
    kmd_token_primary = content[5]
    kmd_address_primary = content[7]
    # salvo anche i parametri degli altri 4 nodi
    algod_token1 = content[9]
    algod_address1 = content[11].replace("[::]", "http://algorand-node")
    kmd_token1 = content[13]
    kmd_address1 = content[15].replace("[::]", "http://algorand-node")
    algod_token2 = content[17]
    algod_address2 = content[19].replace("[::]", "http://algorand-node")
    kmd_token2 = content[21]
    kmd_address2 = content[23].replace("[::]", "http://algorand-node")
    algod_token3 = content[25]
    algod_address3 = content[27].replace("[::]", "http://algorand-node")
    kmd_token3 = content[29]
    kmd_address3 = content[31].replace("[::]", "http://algorand-node")
    algod_token4 = content[33]
    algod_address4 = content[35].replace("[::]", "http://algorand-node")
    kmd_token4 = content[37]
    kmd_address4 = content[39].replace("[::]", "http://algorand-node")

    # now switch default node params
    if len(content) > 39:
        nodenum = int(content[40])
        if nodenum == 0:
            algod_token = algod_token_primary
            algod_address = algod_address_primary
            kmd_token = kmd_token_primary
            kmd_address = kmd_address_primary
        elif nodenum == 1:
            algod_token = algod_token1
            algod_address = algod_address1
            kmd_token = kmd_token1
            kmd_address = kmd_address1
        elif nodenum == 2:
            algod_token = algod_token2
            algod_address = algod_address2
            kmd_token = kmd_token2
            kmd_address = kmd_address2
        elif nodenum == 3:
            algod_token = algod_token3
            algod_address = algod_address3
            kmd_token = kmd_token3
            kmd_address = kmd_address3
        elif nodenum == 4:
            algod_token = algod_token4
            algod_address = algod_address4
            kmd_token = kmd_token4
            kmd_address = kmd_address4
    else:
        algod_token = algod_token_primary
        algod_address = algod_address_primary
        kmd_token = kmd_token_primary
        kmd_address = kmd_address_primary

    if len(content) > 41:
        opt_in_asset_id = content[42]
        opt_out_asset_id = content[44]
