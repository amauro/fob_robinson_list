from . import params
from algosdk import kmd, algod, transaction
from algosdk.wallet import Wallet
from clubuse_ui.models import User

# create a kmd client
def create_wallet():
    kcl = kmd.KMDClient(params.kmd_token, params.kmd_address)

    # create a wallet object
    # infos = kcl.create_wallet("mywallet_name13", "mywallet_password13")
    # print(infos)
    wallet = Wallet("testwallet2", "Testpswd0000!", kcl)
    # get wallet information
    # info = wallet.info()
    print("Wallet name:", wallet)

    # create an account
    address = wallet.generate_key()
    print("New account:", address)
    return address


def initMoney(receiver=None, amount=100000 * 1000 * 1000):
    # 100000 * 1000 * 1000,  # send 100'000 algo
    kcl = kmd.KMDClient(params.kmd_token, params.kmd_address)
    acl = algod.AlgodClient(params.algod_token, params.algod_address)

    wallet = Wallet("unencrypted-default-wallet", "", kcl)

    # find address with maximum money
    max_balance = 0
    account_0 = ""
    for k in wallet.list_keys():
        account_info = acl.account_info(k)
        if account_info["amount"] > max_balance:
            max_balance = account_info["amount"]
            account_0 = k
    # get suggested parameters
    paramsTxn = acl.suggested_params()
    gen = paramsTxn["genesisID"]
    gh = paramsTxn["genesishashb64"]
    last_round = paramsTxn["lastRound"]
    fee = paramsTxn["fee"]

    if receiver is None:
        for u in User.objects.all():
            if u.address:
                # send money to multisgn TAX perdo le fee
                txn_tax = transaction.PaymentTxn(
                    account_0,
                    fee,
                    last_round,
                    last_round + 1000,
                    gh,
                    u.address,
                    amount,
                    note="tax",
                    gen=gen,
                )
                # sign it
                stx = wallet.sign_transaction(txn_tax)
                # send it
                txid = acl.send_transaction(stx)
                print(txid)
    else:
        txn_tax = transaction.PaymentTxn(
            account_0,
            fee,
            last_round,
            last_round + 1000,
            gh,
            receiver,
            amount,
            note="tax",
            gen=gen,
        )
        # sign it
        stx = wallet.sign_transaction(txn_tax)
        # send it
        txid = acl.send_transaction(stx)
        print(txid)

    print("**************SENT")
