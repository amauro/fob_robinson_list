from . import params
import base64
from algosdk import kmd, account, algod, transaction, encoding, mnemonic
from algosdk.wallet import Wallet
from clubuse_ui.cripto_util import generate_privacy_payload
from clubuse_ui.asa_utils.tx_util import send_money


# create a kmd client
def create_wallet(name, pswd, withpk=False, kcl=None):
    if kcl is None:
        kcl = kmd.KMDClient(params.kmd_token, params.kmd_address)

    # generate an account
    private_key, address = account.generate_account()

    # create a wallet object
    infos = kcl.create_wallet(name, pswd)
    print(infos)
    wallet = Wallet(name, pswd, kcl)
    # create an account
    _addrWallet = wallet.import_key(private_key)

    if withpk:
        return address, private_key, mnemonic.from_private_key(private_key)
    else:
        return address


# create standalone account
def generate_algorand_keypair(print_log=False):
    private_key, address = account.generate_account()
    if print_log:
        print("My address: {}".format(address))
        print("My private key: {}".format(private_key))
        print("My passphrase: {}".format(
            mnemonic.from_private_key(private_key)))
    return private_key, address


def get_private_key(username, pswd, address):
    kcl = kmd.KMDClient(params.kmd_token, params.kmd_address)

    wallet = Wallet(username, pswd, kcl)
    print("*********wallet*********")
    print(wallet.info())
    print(address)
    priv = wallet.export_key(address)
    return priv


def get_mnemonic_priv_test(user):
    # default user have same password as wallet name
    kcl = kmd.KMDClient(params.kmd_token, params.kmd_address)
    wallet = Wallet(user.username, user.username, kcl)
    priv = wallet.export_key(user.address)
    mnem = mnemonic.from_private_key(priv)
    return mnem, priv


def ricarica_multi_account(wallet_address, wallet_name, wallet_pswd, receiver):
    kcl = kmd.KMDClient(params.kmd_token, params.kmd_address)
    acl = algod.AlgodClient(params.algod_token, params.algod_address)
    # get suggested parameters
    param_bc = acl.suggested_params()
    gen = param_bc["genesisID"]
    gh = param_bc["genesishashb64"]
    last_round = param_bc["lastRound"]
    fee = param_bc["fee"]
    txn_tax = transaction.PaymentTxn(
        wallet_address,
        fee,
        last_round,
        last_round + 1000,
        gh,
        receiver,
        10 * 1000 * 1000,  # send 10 algo
        note="tax",
        gen=gen,
    )
    # sign it
    wallet_fub = Wallet(wallet_name, wallet_pswd, kcl)
    stx = wallet_fub.sign_transaction(txn_tax)
    # send it
    txid = acl.send_transaction(stx)
    print(txid)
    print("**************SENT")


def get_wallet_info(username, pswd, address):
    kcl = kmd.KMDClient(params.kmd_token, params.kmd_address)

    wallet = Wallet(username, pswd, kcl)
    get_last_block()

    return wallet.export_key(address)


def getAccountInfo(addr, all_info=False):
    acl = algod.AlgodClient(params.algod_token, params.algod_address)
    info = acl.account_info(addr)
    if all_info:
        return info
    # print(info)
    assets = {"OPT_IN": 0, "OPT_OUT": 0}
    if "assets" in info:
        info = info["assets"]
        # print(info)
        for item in info:
            if item == str(params.opt_in_asset_id):
                assets["OPT_IN"] = info[item]["amount"]
            elif item == str(params.opt_out_asset_id):
                assets["OPT_OUT"] = info[item]["amount"]
            # print(info[item])
    return assets


def getBalance(addr):
    acl = algod.AlgodClient(params.algod_token, params.algod_address)
    return acl.account_info(addr)["amount"]


def get_last_block():
    print("here start")
    algod_client = algod.AlgodClient(params.algod_token, params.algod_address)

    status = None
    try:
        status = algod_client.status()
    except Exception as e:
        print("Failed to get algod status: {}".format(e))

    if status:
        print("algod last round: {}".format(status.get("lastRound")))
        print("algod time since last round: {}".format(
            status.get("timeSinceLastRound")))
        print("algod catchup: {}".format(status.get("catchupTime")))
        print("algod latest version: {}".format(
            status.get("lastConsensusVersion")))

    # Retrieve latest block information
    last_round = algod_client.status().get("lastRound")
    print("####################")
    block = algod_client.block_info(last_round)
    print(block)
    return block


def generate_and_send_privacy_payload(private_key, address_attestator,
                                      address_subscriber, msg):
    payload = generate_privacy_payload(private_key, address_attestator,
                                       address_subscriber, msg)
    acl = algod.AlgodClient(params.algod_token, params.algod_address)

    params_bc = acl.suggested_params()
    note = payload.encode()

    data = {
        "sender": address_attestator,
        "receiver": address_subscriber,
        "fee": params_bc.get("minFee"),
        "flat_fee": True,
        "amt": 0,
        "first": params_bc.get("lastRound"),
        "last": params_bc.get("lastRound") + 1000,
        "note": note,
        "gen": params_bc.get("genesisID"),
        "gh": params_bc.get("genesishashb64"),
    }

    txn = transaction.PaymentTxn(**data)
    signed_txn = txn.sign(private_key)
    txid = signed_txn.transaction.get_txid()
    print("Signed transaction with txID: {}".format(txid))
    txid = acl.send_transaction(signed_txn)
    print("SENT")
    return txid
