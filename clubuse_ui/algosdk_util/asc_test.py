from pyteal import *
from algosdk import algod, transaction, account, mnemonic
import binascii


def send_compiled_teal(receiver):
    try:
        # create logic sig
        # hex example b"\x01\x20\x01\x00\x22"
        b64_program = "gqFsxAUBIAEAIqNzaWfEQPXcX4I5rPFshyi3xqvZdziSAIkjNM1rUwP7jRngtgNCDRz0zkRVRY0jVSH3zV9FbOTpBEq3yCASgQhqMUAzyQ8="
        program = binascii.a2b_base64(b64_program)
        print(program)
        lsig = transaction.LogicSig(program)
        print("hereeeeeeeeeeeeeeee")
        sender = lsig.address()
        # create an algod client
        from . import params

        acl = algod.AlgodClient(params.algod_token, params.algod_address)

        # get suggested parameters
        params = acl.suggested_params()
        gen = params["genesisID"]
        gh = params["genesishashb64"]
        last_round = params["lastRound"]
        fee = params["fee"]
        amount = 1 * 1000
        closeremainderto = None

        # create a transaction
        txn = transaction.PaymentTxn(
            sender,
            fee,
            last_round,
            last_round + 100,
            gh,
            receiver,
            amount,
            closeremainderto,
        )
        # Create the LogicSigTransaction with contract account LogicSig
        lstx = transaction.LogicSigTransaction(txn, lsig)

        # send raw LogicSigTransaction to network
        txid = acl.send_transaction(lstx)
        print("Transaction ID: " + txid)
    except Exception as e:
        print(e)


""" Hash Time Locked Contract
"""


def asc_test():
    alice = Addr("6ZHGHH5Z5CTPCF5WCESXMGRSVK7QJETR63M3NY5FJCUYDHO57VTCMJOBGY")
    bob = Addr("7Z5PWO2C6LFNQFGHWKSK5H47IQP5OJW2M3HA2QPXTY3WTNP5NU2MHBW27M")
    secret = Bytes("base32", "23232323232323")

    fee_cond = Txn.fee() < Int(1000)

    type_cond = Txn.type_enum() == Int(1)

    recv_cond = And(
        Txn.close_remainder_to() == Global.zero_address(),
        Txn.receiver() == alice,
        Sha256(Arg(0)) == secret,
    )

    esc_cond = And(
        Txn.close_remainder_to() == Global.zero_address(),
        Txn.receiver() == bob,
        Txn.first_valid() > Int(3000),
    )

    atomic_swap = And(fee_cond, type_cond, Or(recv_cond, esc_cond))

    with open("AlgoConf/example.teal", "w+") as f:
        f.write(atomic_swap.teal())

    print(atomic_swap.teal())


# def send_compile_teal():
# 	program = b"\x01\x20\x01\x00\x22"
#     lsig = transaction.LogicSig(program)
#     txn = transaction.PaymentTxn(contract-address<PLACEHOLDER>, fee<PLACEHOLDER>, first-valid-round<PLACEHOLDER>, last-valid-round<PLACEHOLDER>, genesis-hash<PLACEHOLDER>, receiver<PLACEHOLDER>, amount<PLACEHOLDER>, close-remainder-to-address<PLACEHOLDER>)
#     lstx = transaction.LogicSigTransaction(txn, lsig)
#     txid = acl.send_transaction(lstx)

