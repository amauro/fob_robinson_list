import logging
import time
import json
import threading
import queue
import random
import traceback
import http
import os
import csv
import datetime
import urllib.request
import base64
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from django.core.management.base import BaseCommand
from algosdk.v2client.algod import AlgodClient
from algosdk import algod as algodv1
from algosdk import kmd, error
from algosdk.wallet import Wallet
from clubuse_ui.models import User, StoricoStati
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import *
from django.core.serializers.json import DjangoJSONEncoder
from clubuse_ui.cripto_util import (
    generate_privacy_payload,
    read_privacy_payload,
    isPrivacyPayload,
)

log = logging.getLogger("main")
version_test = "VERSIONE 1.3.1"


class Command(BaseCommand):
    help = "Initialize Users default"

    def add_arguments(self, parser):
        parser.add_argument("-s",
                            "--startnum",
                            type=int,
                            help="Primo numero del range")
        parser.add_argument("-n",
                            "--numinput",
                            type=int,
                            help="Iterazioni dal numero")
        parser.add_argument(
            "-i",
            "--iterations",
            default=1000,
            type=int,
            help="Iterazioni a step, definisci il numero di passi per step",
        )
        parser.add_argument(
            "-f",
            "--filename",
            default="prune_log",
            type=str,
            help="Nome del file da esportare",
        )
        parser.add_argument(
            "-p",
            "--print",
            default=False,
            action="store_true",
            help="Print time each phase",
        )
        parser.add_argument(
            "-d",
            "--db",
            default=False,
            action="store_true",
            help="Read Using DB",
        )

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        prune_user_test(
            options["startnum"],
            options["numinput"],
            options["iterations"],
            options["print"],
            options["db"],
            options["filename"],
        )


def prune_user_test(startnum, numinput, step, print_step, read_db, filename):
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    # parameter query
    acl = AlgodClient(params.algod_token, params.algod_address)
    fub_account = User.objects.get(username="attestator")
    fub_addr = fub_account.address
    # WITH FUB, I LOAD WALLET WORKAROUND
    fub_private_key = wallet_util.get_private_key(fub_account.username,
                                                  fub_account.username,
                                                  fub_addr)

    # get storico list to analize
    start = 0
    total_count = 12850
    if startnum:
        start = startnum
    if numinput:
        total_count = numinput
    # nella realtà leggi un csv, ma siccome è costante questo tempo, leggi da DB
    storico_list = (StoricoStati.objects.filter(
        data__date="2011-02-02",
        data__hour="14").order_by("data").distinct().all()[start:total_count])
    # 12899 elementi max il 2011-02-02 alle ore 14

    # debug info
    print("******")
    print(version_test)
    print("Test da %s a %s numeri di telefono." %
          (str(start), str(total_count)))
    print("******")

    # write test on file csvs
    output = csv.writer(open(filename + ".csv", "w"))
    output.writerow(["status", "phone", "found", "time"])
    output.writerow(["START ALL", "---", "---", datetime.datetime.now()])

    # PERFORM TEST

    # prepare list num
    output.writerow([
        "START POPULATE ADDRESS-PHONE", "---", "---",
        datetime.datetime.now()
    ])
    list_consesus = {}
    for s in storico_list:
        list_consesus[s.codifica] = 0
    output.writerow(
        ["END POPULATE ADDRESS-PHONE", "---", "---",
         datetime.datetime.now()])

    # get whole bc
    if not read_db:
        output.writerow(
            ["START READ BLOCKCHAIN", "---", "---",
             datetime.datetime.now()])
        payloads = _getTrxList(fub_addr, step)
        output.writerow(
            ["END READ BLOCKCHAIN", "---", "---",
             datetime.datetime.now()])

        output.writerow(
            ["START PRUNE PHONE", "---", "---",
             datetime.datetime.now()])
        # read back
        for payload in payloads:
            if isPrivacyPayload(payload):
                decrypted = read_privacy_payload(payload,
                                                 fub_addr,
                                                 fub_private_key,
                                                 attestator=True)
                for s in storico_list:
                    mob_phone = str(s.codifica)
                    # id è il numero di telefono importato per i test
                    if decrypted["phone"] == mob_phone:
                        output.writerow([
                            "START PRUNING SINGLE PHONE",
                            mob_phone,
                            decrypted["address"],
                            datetime.datetime.now(),
                        ])
                        # read and store state
                        list_consesus[mob_phone] = _getstate(
                            decrypted["address"])
                        output.writerow([
                            "END PRUNING SIGLE PHONE",
                            mob_phone,
                            decrypted["address"],
                            datetime.datetime.now(),
                        ])
                        break
        output.writerow(
            ["END PRUNE PHONE", "---", "---",
             datetime.datetime.now()])
    else:
        output.writerow(
            ["START PRUNE PHONE", "---", "---",
             datetime.datetime.now()])
        for s in storico_list:
            mob_phone = str(
                s.codifica)  # id è il numero di telefono importato per i test
            try:
                address = User.objects.get(mob_phone=mob_phone).address
                output.writerow([
                    "START PRUNING SINGLE PHONE",
                    mob_phone,
                    address,
                    datetime.datetime.now(),
                ])
                # read and store state
                list_consesus[mob_phone] = _getstate(address)
                output.writerow([
                    "END PRUNING SIGLE PHONE",
                    mob_phone,
                    address,
                    datetime.datetime.now(),
                ])
            except User.DoesNotExist:
                pass  # not stored in db
        output.writerow(
            ["END PRUNE PHONE", "---", "---",
             datetime.datetime.now()])

    output.writerow(["END ALL", "---", "---", datetime.datetime.now()])

    print(list_consesus)


def _getTrxList(address_fub, step):
    algod_client = algodv1.AlgodClient(params.algod_token,
                                       params.algod_address)

    txns = {"transactions": []}

    # receive from last to first
    last_round = algod_client.status().get("lastRound")
    # print(last_round)
    txns_list = []
    for i_step in range(0, last_round, step):
        # print(i_step, i_step + step)
        last_step = i_step + step
        if last_step > last_round:
            last_step = last_round
        txns = algod_client.transactions_by_address(
            address_fub,
            first=i_step,
            last=last_step,
            limit=10**9,
            from_date=None,
            to_date=None,
        )
        txns_list.append(txns)
    payloads = []
    for txns in txns_list:
        if "transactions" in txns:
            for txn in reversed(txns["transactions"]):
                item = {}
                if "noteb64" in txn:
                    payloads.append(base64.b64decode(txn["noteb64"]).decode())
                payloads.append(item)
    return payloads


def _getstate(address):
    payload = wallet_util.getAccountInfo(address)
    num = -1
    opt_in = payload["OPT_IN"]
    opt_out = payload["OPT_OUT"]
    if opt_in == 0 and opt_out == 0:
        num = -2
    elif opt_in == 1 and opt_out == 0:
        num = 1
    elif opt_in == 0 and opt_out == 1:
        num = 0
    else:
        print(opt_in, opt_out, address)
    return num
