import logging
import time
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from django.core.management.base import BaseCommand
from clubuse_ui.models import User
from clubuse_ui.views import transfer_asa
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import optin_asa
from algosdk.v2client.algod import AlgodClient

opt_out_asset_id = int(params.opt_out_asset_id)
opt_in_asset_id = int(params.opt_in_asset_id)
# WITH FUB, I LOAD WALLET WORKAROUND
fub_account = User.objects.get(username="attestator")
fub_addr = fub_account.address
fub_private_key = wallet_util.get_private_key(
    fub_account.username, fub_account.username, fub_addr
)

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Query Pruning Test"

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        query_pruning_test()


def query_pruning_test():
    # generate random list
    start = time.time()

    mob_phone = 3333333330
    for i in range(1000):
        num = mob_phone + i
        _getstate(User.objects.get(mob_phone=num).address)

    end = time.time() - start
    print("finished*******")
    print(end)


def _getstate(address):
    payload = wallet_util.getAccountInfo(address)
    num = -1
    opt_in = payload["OPT_IN"]
    opt_out = payload["OPT_OUT"]
    if opt_in == 0 and opt_out == 0:
        num = -2
    elif opt_in == 1 and opt_out == 0:
        num = 1
    elif opt_in == 0 and opt_out == 1:
        num = 0
    else:
        print(opt_in, opt_out, address)
    return num
