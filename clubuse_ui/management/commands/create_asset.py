import logging
from clubuse_ui.algosdk_util import wallet_util
from django.core.management.base import BaseCommand
from clubuse_ui.models import User
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import *
from clubuse_ui.algosdk_util import params, wallet_util
from algosdk.wallet import Wallet
from algosdk import algod, kmd, transaction

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Create token OPTIN and OPTOUT"

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        create_assets()


def create_assets():
    acl = algod.AlgodClient(params.algod_token, params.algod_address)

    fub_account = User.objects.get(username="attestator")
    fub_addr = fub_account.address
    # WITH FUB INIT SCRIPT, I LOAD WALLET WORKAROUND
    fub_private_key = wallet_util.get_private_key(
        fub_account.username, fub_account.username, fub_addr
    )

    opt_out_asset_id = create_asa(
        acl, fub_addr, fub_private_key, "OPTOUT", total_asset=1000000000
    )  # 1Bilion
    opt_in_asset_id = create_asa(
        acl, fub_addr, fub_private_key, "OPTIN", total_asset=1000000000
    )  # 1Bilion
    print("OPTIN %s --- OPTOUT %s" % (opt_in_asset_id, opt_out_asset_id))
    # open file node_conf and append
    with open("node_conf", "a") as conf:
        conf.write("opt_in_asset_id\n")
        conf.write(str(opt_in_asset_id) + "\n")
        conf.write("opt_out_asset_id\n")
        conf.write(str(opt_out_asset_id) + "\n")
