import logging
import time
import json
import threading
import queue
import random
import traceback
import http
import os
import csv
import datetime
import urllib.request
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from django.core.management.base import BaseCommand
from algosdk.v2client.algod import AlgodClient
from algosdk import algod as algodv1
from algosdk import kmd, error
from algosdk.wallet import Wallet
from clubuse_ui.models import User, StoricoStati
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import *
from django.core.serializers.json import DjangoJSONEncoder

log = logging.getLogger("main")
version_test = "VERSIONE 1.8.2"
queue_list = {
    "0": queue.Queue(),
    "1": queue.Queue(),
    "2": queue.Queue(),
    "3": queue.Queue(),
    "4": queue.Queue(),
}


class Command(BaseCommand):
    help = "Initialize Users default"

    def add_arguments(self, parser):
        parser.add_argument("-s",
                            "--startnum",
                            type=int,
                            help="Primo numero del range")
        parser.add_argument("-n",
                            "--numinput",
                            type=int,
                            help="Iterazioni dal numero")
        parser.add_argument("-t",
                            "--threadnum",
                            type=int,
                            help="Numero di thread")
        parser.add_argument("-k",
                            "--kmdnodes",
                            type=int,
                            help="Numero di Nodi KMD")
        parser.add_argument(
            "-p",
            "--print",
            default=False,
            action="store_true",
            help="Print time each phase",
        )
        parser.add_argument(
            "-a",
            "--standaloneaccount",
            default=True,
            action="store_true",
            help="Create standalone account, not wallet",
        )

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        create_users_async(
            options["startnum"],
            options["numinput"],
            options["threadnum"],
            options["kmdnodes"],
            options["print"],
            options["standaloneaccount"],
        )


def create_users_async(startnum, numinput, thread_num, kmdnodes, print_step,
                       standalone_account):
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    # 1 instance of acl and all the 5 instances of kcl
    acl = AlgodClient(params.algod_token, params.algod_address)
    kcl_primary = kmd.KMDClient(params.kmd_token, params.kmd_address)
    kcl1 = kmd.KMDClient(params.kmd_token1, params.kmd_address1)
    kcl2 = kmd.KMDClient(params.kmd_token2, params.kmd_address2)
    kcl3 = kmd.KMDClient(params.kmd_token3, params.kmd_address3)
    kcl4 = kmd.KMDClient(params.kmd_token4, params.kmd_address4)
    list_kcl = [kcl_primary, kcl1, kcl2, kcl3, kcl4]

    # parameters: assets and address attestator
    opt_out_asset_id = int(params.opt_out_asset_id)
    opt_in_asset_id = int(params.opt_in_asset_id)
    fub_account = User.objects.get(username="attestator")
    fub_addr = fub_account.address
    # WITH FUB, I LOAD WALLET WORKAROUND
    fub_private_key = wallet_util.get_private_key(fub_account.username,
                                                  fub_account.username,
                                                  fub_addr)

    # get storico list to analize
    storico_list = (StoricoStati.objects.filter(
        data__date="2011-02-02", data__hour="14").order_by("data").distinct())
    # 12850 elementi a step di 5
    start = 0
    total_count = 12850
    step_size = 5
    thread_count = 1
    kmd_count = 5
    if startnum:
        start = startnum
    if numinput:
        total_count = numinput
    if thread_num:
        thread_count = thread_num
    if kmdnodes:
        kmd_count = kmdnodes

    # write test on file csvs
    output = csv.writer(open("export_log.csv", "w"))
    output.writerow(["thread", "kmd", "status", "time"])
    output.writerow(["Main", "---", "START ALL", datetime.datetime.now()])

    # debug info
    print("******")
    print(version_test)
    print("Test da %s a %s inserimenti." % (str(start), str(total_count)))
    print("Utilizzati %s thread per coda ed un numero di nodi kmd pari a %s." %
          (str(thread_count), str(kmd_count)))
    print("******")

    # populate the five ques
    print("..start preparing queue..")
    # if standalone account, no use of kmd and so speed up
    print(standalone_account)
    if standalone_account:
        kmd_count = 1
        for storico in storico_list[start:total_count]:
            queue_list["0"].put(storico)
    else:
        for index in range(start, total_count,
                           kmd_count):  # kmd count represnt step size
            round = 0
            for storico in storico_list[index:index + kmd_count]:
                queue_list[str(round)].put(storico)
                round += 1
    print("..end preparing queue..")

    # create 5 daemon thread, each one should cosume from queue
    print("..START PREPARING %s daemon thread.." % kmd_count)
    for kcl_pos in range(kmd_count):
        threading.Thread(
            target=creation_user_step,
            args=(
                opt_in_asset_id,
                opt_out_asset_id,
                acl,
                fub_addr,
                fub_private_key,
                list_kcl[kcl_pos],
                kcl_pos,
                output,
                print_step,
                standalone_account,
            ),
            daemon=True,
        ).start()
    print("..END PREPARING %s daemon thread.." % kmd_count)

    print(queue_list)
    # wait the end of all threads
    for _round in range(kmd_count):
        queue_list[str(_round)].join()

    output.writerow(["Main", "---", "END ALL", datetime.datetime.now()])


def creation_user_step(
    opt_in_asset_id,
    opt_out_asset_id,
    acl,
    fub_addr,
    fub_private_key,
    kcl,
    kcl_pos,
    output,
    print_step,
    standalone_account,
):
    while True:
        storico = queue_list[str(kcl_pos)].get()
        username = storico.codifica
        output.writerow([
            "Thread " + username, kcl.kmd_token, "STARTING",
            datetime.datetime.now()
        ])
        while True:
            try:
                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "START CREATE WALLET",
                        datetime.datetime.now(),
                    ])
                if standalone_account:
                    print("standalone***")
                    user_private_key, address = wallet_util.generate_algorand_keypair(
                        print_log=True)

                else:
                    try:
                        # TODO SPLITTA LE RICHIESTE
                        # create wallet
                        (
                            address,
                            user_private_key,
                            _mnemtopriv,
                        ) = wallet_util.create_wallet(username,
                                                      username,
                                                      withpk=True,
                                                      kcl=kcl)
                    except:
                        # wallet already created so load info (KNOWING INFO) FEATURE ONLY FOR TEST
                        wallet = Wallet(username, username, kcl)
                        address = wallet.list_keys(
                        )[0]  # index error means wallet not created, so retry, MAYBE KMD DOWN PREVIOUSLY
                        user_private_key = wallet.export_key(address)
                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "END CREATE WALLET",
                        datetime.datetime.now(),
                    ])

                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "START CREATE USER",
                        datetime.datetime.now(),
                    ])
                if User.objects.filter(username=username).count() < 1:
                    # user not created so create
                    user = User.objects.create_user(
                        username=username,
                        password=username,
                        address=address,
                        mob_phone=str(storico.id),  # or codifica so username ?
                    )
                    user.save()
                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "END CREATE USER",
                        datetime.datetime.now(),
                    ])

                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "START INIT MONEY",
                        datetime.datetime.now(),
                    ])
                # initialize money in just created wallet
                txn = make_pay_transaction(acl, fub_addr, address, 1000000)
                stxn = txn.sign(fub_private_key)
                try:
                    acl.send_transaction(stxn)
                except:
                    algod_token = acl.algod_token
                    algod_address = acl.algod_address
                    acl1 = algodv1.AlgodClient(algod_token, algod_address)
                    acl1.send_transaction(stxn)
                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "END INIT MONEY",
                        datetime.datetime.now(),
                    ])

                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "START CIPHER NOTE",
                        datetime.datetime.now(),
                    ])
                # registration payload on chain crypt
                msg = {"address": address, "phone": username}
                wallet_util.generate_and_send_privacy_payload(
                    fub_private_key,
                    fub_addr,
                    address,
                    json.dumps(msg,
                               sort_keys=True,
                               indent=1,
                               cls=DjangoJSONEncoder),
                )
                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "END CIPHER NOTE",
                        datetime.datetime.now(),
                    ])

                # optin asa
                for asset_id in (opt_in_asset_id, opt_out_asset_id):
                    if print_step:
                        output.writerow([
                            "Thread " + username,
                            kcl.kmd_token,
                            "START OPTIN ASA " + str(opt_in_asset_id),
                            datetime.datetime.now(),
                        ])
                    optin_asa(acl,
                              address,
                              user_private_key,
                              asset_id,
                              skip_unusefull=True)
                    if print_step:
                        output.writerow([
                            "Thread " + username,
                            kcl.kmd_token,
                            "END OPTIN ASA " + str(opt_in_asset_id),
                            datetime.datetime.now(),
                        ])

                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "START TRANSFER ASA",
                        datetime.datetime.now(),
                    ])
                # receive asa from attestator account
                transfer_asa(
                    acl,
                    fub_addr,
                    address,
                    fub_private_key,
                    1,
                    opt_out_asset_id,
                    skip_unusefull=True,
                )
                if print_step:
                    output.writerow([
                        "Thread " + username,
                        kcl.kmd_token,
                        "END  TRANSFER ASA",
                        datetime.datetime.now(),
                    ])
                break
            except Exception:
                logging.exception("Generic error")
                time.sleep(3)  # 3 > 2 (kmd sleeps 2 sec if down)
        output.writerow([
            "Thread " + username, kcl.kmd_token, "FINISHING",
            datetime.datetime.now()
        ])
        queue_list[str(kcl_pos)].task_done()
        if queue_list[str(kcl_pos)].empty():
            break  # end thread


def _ping_url(url, output):
    output.writerow(["Thread " + url, "STARTING", datetime.datetime.now()])
    while True:
        try:
            page = urllib.request.urlopen(url)
            print(page.getcode())
            break
        except:
            pass
    output.writerow(["Thread " + url, "FINISHING", datetime.datetime.now()])
