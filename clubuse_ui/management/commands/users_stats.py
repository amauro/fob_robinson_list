import logging
import time
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from django.core.management.base import BaseCommand
from django.db.models import Count
from django.db.models.functions import TruncDay
from algosdk.v2client.algod import AlgodClient
from clubuse_ui.models import User, StoricoStati
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import *


log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Initialize Users default"

    def add_arguments(self, parser):
        parser.add_argument(
            "-s", "--startnum", type=int, help="Primo numero di telefono del range"
        )
        parser.add_argument(
            "-n", "--numinput", type=int, help="Iterazioni dal numero di telefono"
        )

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        users_stats()


def users_stats():
    # generate random list
    ret = (
        StoricoStati.objects.values("data__date")
        .annotate(count=Count("id"))
        .values("data__date", "count")
        .order_by("-count")
    )
    print(ret[:10])
    # max datetime.date(2011, 2, 2)
    # print(StoricoStati.objects.all().count())
