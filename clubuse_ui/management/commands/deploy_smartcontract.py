import logging
from clubuse_ui.algosdk_util import wallet_util
from django.core.management.base import BaseCommand
from clubuse_ui.models import User
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import *
from clubuse_ui.algosdk_util import params, wallet_util
from algosdk.wallet import Wallet
from algosdk import algod, kmd, transaction
from clubuse_ui.algosdk_util import wallet_init

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Deploy smart contract, send all OPTIN tokens and some OPTOUT"

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        deploy_smart_contract()


def deploy_smart_contract():
    acl = algod.AlgodClient(params.algod_token, params.algod_address)

    fub_account = User.objects.get(username="attestator")
    fub_addr = fub_account.address

    # WITH FUB INIT SCRIPT, I LOAD WALLET WORKAROUND
    fub_private_key = wallet_util.get_private_key(
        fub_account.username, fub_account.username, fub_addr
    )
    opt_out_asset_id = int(params.opt_out_asset_id)
    opt_in_asset_id = int(params.opt_in_asset_id)

    # # retrieve and deploy smart contract
    program = open("opt_in_out_sc.lsig", "rb").read()
    sc_addr = transaction.LogicSig(program).address()
    print("Smart contract deployed --- Add: %s" % (sc_addr))

    # refill algo sc_add
    wallet_init.initMoney(receiver=sc_addr)

    # smart contract optin assets
    optin_asa_sc(acl, sc_addr, program, opt_in_asset_id)
    optin_asa_sc(acl, sc_addr, program, opt_out_asset_id)
    print("optin smart contract account done*********")

    # transfer assets to the SC
    transfer_asa(
        acl, fub_addr, sc_addr, fub_private_key, 1000000, opt_out_asset_id
    )  # transfer tot the SC 1 MILION
    transfer_asa(
        acl, fub_addr, sc_addr, fub_private_key, 1000000000, opt_in_asset_id
    )  # tranfer to the SC ALL 1 BILION

    print("asa trasf to sc done*********")

