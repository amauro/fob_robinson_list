import logging
from clubuse_ui.algosdk_util import wallet_util
from django.core.management.base import BaseCommand
from clubuse_ui.models import User
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import *

from clubuse_ui.asa_utils.sc import make
from clubuse_ui.algosdk_util import params, wallet_util
from algosdk.wallet import Wallet
from algosdk import algod, kmd, transaction

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Compile Smart Contract"

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        create_sc()


def create_sc():
    fub_addr = User.objects.get(username="attestator").address
    opt_out = int(params.opt_out_asset_id)
    opt_in = int(params.opt_in_asset_id)
    res = make(fub_addr, opt_out, opt_in)
    with open("opt_in_out_sc.teal", "w") as sc:
        sc.write(res)

