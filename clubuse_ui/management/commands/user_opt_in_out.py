import logging
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init, asc_test
from clubuse_ui.asa_utils.tx_util import *
from django.core.management.base import BaseCommand
from algosdk.v2client.algod import AlgodClient
from clubuse_ui.models import User
from django.contrib.auth import authenticate

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "User Opt in Out test"

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        opt_users()


def opt_users():
    opt_out_asset_id = int(params.opt_out_asset_id)
    opt_in_asset_id = int(params.opt_in_asset_id)
    for u in User.objects.all():
        if u.username == "attestator":
            continue  # skip

        # optin assets
        acl = AlgodClient(params.algod_token, params.algod_address)
        account_info = acl.account_info(u.address)
        for asset_id in (opt_in_asset_id, opt_out_asset_id):
            holding = None
            if "assets" in account_info:
                holding = account_info["assets"].get(str(asset_id))

            if not holding:
                amount = wallet_util.getAccountInfo(u.address, all_info=True)["amount"]
                if amount < 1000:  # less than 1 Algo, so just created
                    wallet_init.initMoney(receiver=u.address)
                _mnem, user_private_key = wallet_util.get_mnemonic_priv_test(u)
                optin_asa(acl, u.address, user_private_key, asset_id)

        text = wallet_util.getAccountInfo(u.address)
        if text["OPT_IN"] == 0 and text["OPT_OUT"] == 0:
            # BLACK BOX SUPPOSE NO VALIDATION
            # transfer Opt-out to the user
            fub_account = User.objects.get(username="attestator")
            fub_addr = fub_account.address

            # WITH FUB, I LOAD WALLET WORKAROUND
            fub_private_key = wallet_util.get_private_key(
                fub_account.username, fub_account.username, fub_addr
            )
            transfer_asa(
                acl, fub_addr, u.address, fub_private_key, 1, opt_out_asset_id
            )  # OPT-OUT to USER
            print("OPT OUT DONE FOR FIRST USER %s" % (u))
        else:
            print("OPT OUT SKIPPED FOR FIRST USER %s" % (u))
