import logging
import time
import json
import threading
import queue
import random
import traceback
import http
import os
import csv
import datetime
import urllib.request
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from django.core.management.base import BaseCommand
from algosdk.v2client.algod import AlgodClient
from algosdk import algod as algodv1
from algosdk import kmd, error
from algosdk.wallet import Wallet
from clubuse_ui.models import User, StoricoStati
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import *
from django.core.serializers.json import DjangoJSONEncoder
from clubuse_ui.algosdk_util import params

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Change default node params"

    def add_arguments(self, parser):
        parser.add_argument(
            "-n",
            "--nodenum",
            type=int,
            help="Switch to node 0 to 5, 0 is default primary relay",
        )

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        switch_node(options["nodenum"])


def switch_node(nodenum):
    print("change to node %s START" % nodenum)

    # read the file into a list of lines
    lines = open("node_conf", "r").readlines()
    lines[-5] = str(nodenum) + "\n"  # 4 lines assets
    # now write the modified list back out to the file
    open("node_conf", "w").writelines(lines)

    # if nodenum == 0:
    #     params.algod_token = params.algod_token_primary
    #     params.algod_address = params.algod_address_primary
    #     params.kmd_token = params.kmd_token_primary
    #     params.kmd_address = params.kmd_address_primary
    # elif nodenum == 1:
    #     params.algod_token = params.algod_token1
    #     params.algod_address = params.algod_address1
    #     params.kmd_token = params.kmd_token1
    #     params.kmd_address = params.kmd_address1
    # elif nodenum == 2:
    #     params.algod_token = params.algod_token2
    #     params.algod_address = params.algod_address2
    #     params.kmd_token = params.kmd_token2
    #     params.kmd_address = params.kmd_address2
    # elif nodenum == 3:
    #     params.algod_token = params.algod_token3
    #     params.algod_address = params.algod_address3
    #     params.kmd_token = params.kmd_token3
    #     params.kmd_address = params.kmd_address3
    # elif nodenum == 4:
    #     params.algod_token = params.algod_token4
    #     params.algod_address = params.algod_address4
    #     params.kmd_token = params.kmd_token4
    #     params.kmd_address = params.kmd_address4
    if nodenum < 0 or nodenum > 4:
        print("invalid num range 0-4")
    print(params.algod_token, params.algod_address)
    print(params.kmd_token, params.kmd_address)
    print("change to node %s DONE" % nodenum)
