import logging
import time
from clubuse_ui.algosdk_util import wallet_util
from django.core.management.base import BaseCommand
from clubuse_ui.models import User
from django.contrib.auth import authenticate

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Initialize Users default"

    def add_arguments(self, parser):
        parser.add_argument(
            "-s", "--startnum", type=int, help="Primo numero di telefono del range"
        )
        parser.add_argument(
            "-n", "--numinput", type=int, help="Iterazioni dal numero di telefono"
        )
        parser.add_argument(
            "-u",
            "--users",
            default=False,
            action="store_true",
            help="Create random users too NOT AUTHENTICATED",
        )

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        initial_num = options["startnum"]
        iterations = options["numinput"]

        if initial_num is not None and iterations is not None:
            create_users(
                onlyRandom=True,
                initial_num=initial_num,
                iterations=iterations,
                users=options["users"],
            )
        elif iterations is not None:
            create_users(onlyRandom=True, iterations=iterations, users=options["users"])
        elif User.objects.filter(username="attestator").exists():
            print("DATABASE GIA INIZIALIZZATO")
        else:
            create_users(users=options["users"])


def create_users(onlyRandom=False, initial_num=3333333330, iterations=10, users=False):
    # generate random list
    index = 1
    start = time.time()
    if users:
        for num in range(initial_num, initial_num + iterations):
            start_single = time.time()
            username = "user" + str(index)
            address = wallet_util.create_wallet(username, username)
            user = User.objects.create_user(
                username=username,
                password=username,
                address=address,
                mob_phone=str(num),
            )
            user.save()
            end_single = time.time() - start_single
            print("finished SINGLE *******")
            print(end_single)
            index += 1
    # utente amministratore
    if not onlyRandom:
        username = "attestator"
        address = wallet_util.create_wallet(username, username)
        user = User.objects.create_user(
            username=username, password=username, address=address
        )
        user.is_superuser = True
        user.is_staff = True
        user.save()

    end = time.time() - start
    print("finished*******")
    print(end)
