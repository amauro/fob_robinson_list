from django.core.management.base import BaseCommand
import base64
import logging
import json
import requests
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from clubuse_ui.models import User
from clubuse_ui.asa_utils.tx_util import wait_for_confirmation
from algosdk.v2client.algod import AlgodClient
from algosdk import algod as algodv1
from algosdk import encoding
from algosdk.transaction import SignedTransaction
from django.core.serializers.json import DjangoJSONEncoder
from clubuse_ui.cripto_util import isPrivacyPayload

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Test API"

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        test_api()


def test_api():
    """Full test on api from registration to node switch"""
    acl = AlgodClient(params.algod_token, params.algod_address)
    user_private_key, address = wallet_util.generate_algorand_keypair(
        print_log=True)
    if user_private_key is None:
        raise Exception("private key None")
    if address is None:
        raise Exception("address None")

    # REGISTER API
    url = "http://localhost:8081/api/register"
    payload = {"addr": address, "mobile": "123456789"}
    headers = {"Content-type": "application/json"}
    resp = requests.post(url, json=payload, headers=headers)
    data = resp.json()
    register_tx = data["txid"]
    if register_tx is None:
        raise Exception("register_tx None")
    wait_for_confirmation(acl, register_tx)

    # ORA CHIAMO OPTIN
    url = "http://localhost:8081/api/optin_request"
    resp = requests.post(url, json=payload, headers=headers)
    data = resp.json()
    txn1 = data["txn1"]
    txn2 = data["txn2"]
    txn1 = encoding.msgpack_decode(txn1)
    txn1 = txn1.sign(user_private_key)
    txn2 = encoding.msgpack_decode(txn2)
    txn2 = txn2.sign(user_private_key)
    if not isinstance(txn1, SignedTransaction):
        raise Exception("txn1 NOT signed")
    if not isinstance(txn2, SignedTransaction):
        raise Exception("txn2 NOT signed")

    # ORA INVIO LE TRANSAZIONI FIRMATE
    url = "http://localhost:8081/api/send_trx_optin"
    payload = {
        "signed_txn1": encoding.msgpack_encode(txn1),
        "signed_txn2": encoding.msgpack_encode(txn2),
        "addr": address,
    }
    resp = requests.post(url, json=payload, headers=headers)
    data = resp.json()
    state_tx = data["txid"]
    if state_tx is None:
        raise Exception("state_tx None")
    wait_for_confirmation(acl, state_tx)

    # NOW CHECK INFO USER
    url = "http://localhost:8081/api/user_info"
    payload = {
        "addr": address,
    }
    resp = requests.post(url, json=payload, headers=headers)
    data = resp.json()
    opt_in = data["account"]["OPT_IN"]
    opt_out = data["account"]["OPT_OUT"]
    txn_assed_id = data["txn_assed_id"]
    tx_reg_id = data["tx_reg_id"]
    timestamp = data["timestamp"]
    if not opt_in == 0:
        raise Exception("opt in not 0")
    if not opt_out == 1:
        raise Exception("opt out not 1")
    if not tx_reg_id == register_tx:
        raise Exception("register_tx different from tx_reg_id")
    if not txn_assed_id == state_tx:
        raise Exception("txn_assed_id different from state_tx")
    if timestamp is None:
        raise Exception("timestamp is None")

    # chiedi txn cambio stato
    url = "http://localhost:8081/api/toggle_state"
    payload = {
        "addr": address,
    }
    resp = requests.post(url, json=payload, headers=headers)
    data = resp.json()
    txn1 = data["txn1"]
    txn2 = data[
        "txn2"]  # non la tocco, la lascio come risposta potrebbe controllare la firm il device
    txn1 = encoding.msgpack_decode(txn1)
    txn1 = txn1.sign(user_private_key)
    if not isinstance(txn1, SignedTransaction):
        raise Exception("txn1 NOT signed")

    # ora invia le transazioni quindi cambia stato
    url = "http://localhost:8081/api/send_trx_state"
    payload = {
        "signed_txn1": encoding.msgpack_encode(txn1),
        "signed_txn2": txn2,
    }
    resp = requests.post(url, json=payload, headers=headers)
    data = resp.json()
    state_tx = data["txid"]
    if state_tx is None:
        raise Exception("state_tx is None")
    wait_for_confirmation(acl, state_tx)

    # NOW CHECK INFO USER
    url = "http://localhost:8081/api/user_info"
    payload = {
        "addr": address,
    }
    resp = requests.post(url, json=payload, headers=headers)
    data = resp.json()
    opt_in = data["account"]["OPT_IN"]
    opt_out = data["account"]["OPT_OUT"]
    txn_assed_id = data["txn_assed_id"]
    tx_reg_id = data["tx_reg_id"]
    timestamp = data["timestamp"]
    if not opt_in == 1:
        raise Exception("opt in not 1")
    if not opt_out == 0:
        raise Exception("opt out not 0")
    if not tx_reg_id == register_tx:
        raise Exception("register_tx different from tx_reg_id")
    if not txn_assed_id == state_tx:
        raise Exception("txn_assed_id different from state_tx")
    if timestamp is None:
        raise Exception("timestamp is None")
