import logging
import time
import json
from django.core.serializers.json import DjangoJSONEncoder
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from django.core.management.base import BaseCommand
from clubuse_ui.models import User
from clubuse_ui.views import transfer_asa
from django.contrib.auth import authenticate
from clubuse_ui.asa_utils.tx_util import optin_asa
from algosdk.v2client.algod import AlgodClient

opt_out_asset_id = int(params.opt_out_asset_id)
opt_in_asset_id = int(params.opt_in_asset_id)
# WITH FUB, I LOAD WALLET WORKAROUND
fub_account = User.objects.get(username="attestator")
fub_addr = fub_account.address
fub_private_key = wallet_util.get_private_key(
    fub_account.username, fub_account.username, fub_addr
)

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Initialize Users OPT OUT TOKEN"

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        opt_out()


def opt_out():
    # generate random list
    start = time.time()

    for u in User.objects.all():
        if u.username == "attestator":
            continue
        start_single = time.time()
        print(u)
        # now send payload in chain
        wallet_init.initMoney(receiver=u.address)
        address_attestator = User.objects.get(username="attestator").address
        msg = {"address": u.address, "phone": u.mob_phone}
        pk = wallet_util.get_private_key(u.username, u.username, u.address)  # OVER HEAD
        wallet_util.generate_and_send_privacy_payload(
            pk,
            address_attestator,
            u.address,
            json.dumps(msg, sort_keys=True, indent=1, cls=DjangoJSONEncoder),
        )
        optin_user_asa(u)
        end_single = time.time() - start_single
        print("finished SINGLE *******")
        print(end_single)

    end = time.time() - start
    print("finished*******")
    print(end)


def optin_user_asa(user):
    acl = AlgodClient(params.algod_token, params.algod_address)
    account_info = acl.account_info(user.address)
    for asset_id in (opt_in_asset_id, opt_out_asset_id):
        holding = None
        if "assets" in account_info:
            holding = account_info["assets"].get(str(asset_id))

        if not holding:
            user_private_key = wallet_util.get_private_key(
                user.username, user.username, user.address
            )  # OVER HEAD
            # amount = wallet_util.getAccountInfo(user.address, all_info=True)[
            #     "amount"
            # ]
            # if amount < 1000:  # less than 1 Algo, so just created
            #     wallet_init.initMoney(receiver=user.address)
            optin_asa(
                acl, user.address, user_private_key, asset_id, skip_unusefull=True
            )

    text = wallet_util.getAccountInfo(user.address)
    if text["OPT_IN"] == 0 and text["OPT_OUT"] == 0:
        transfer_asa(
            acl,
            fub_addr,
            user.address,
            fub_private_key,
            1,
            opt_out_asset_id,
            skip_unusefull=True,
        )  # OPT-OUT to USER

    return True
