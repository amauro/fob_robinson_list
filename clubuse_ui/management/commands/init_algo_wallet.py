import logging
from clubuse_ui.algosdk_util import wallet_util
from django.core.management.base import BaseCommand
from clubuse_ui.models import User
from django.contrib.auth import authenticate
from clubuse_ui.algosdk_util import wallet_init

log = logging.getLogger("main")


class Command(BaseCommand):
    help = "Initialize Users with Algo coins"

    def handle(self, *args, **options):
        verbosity = int(options["verbosity"])
        root_logger = logging.getLogger("")
        if verbosity > 1:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity == 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger

        wallet_init.initMoney()  # SEND INITIAL MONEY TO ALL ACCOUNT
        fub_addr = User.objects.get(username="attestator").address
        amount = 1000 * 1000 * 1000 * 1000 * 1000  # 1 bilion
        wallet_init.initMoney(receiver=fub_addr, amount=amount)
