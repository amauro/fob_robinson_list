jQuery(document).ready(function ($) {
    $('button.read-payload').click(function () {
        var pre = $(this).closest('td').find('pre').first().html()
        console.log(pre)
        $('#note-payload').val(pre)
        console.log($('#note-payload'))
        $('#tx-id').val($(this).data('txn'))
        $('#wallet-address').val($(this).data('sender'))
        //set id
        console.log($(this).closest('td'))
        $(this).closest('td').attr('id', 'decrypted');
    });
    $('button.read-info').click(function () {
        $('#wallet').html('..')
        $('#reg_txn').html('..')
        $('#status_txn').html('..')
        $('#time_status_txn').html('..')
        var xmlHttp = new XMLHttpRequest();
        address = $(this).closest('tr').find('td.wallet-info').html()
        url = $('#urlapp').text()
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                payload = JSON.parse(xmlHttp.responseText);
                console.log(payload)
                $('#wallet').html(address)
                $('#reg_txn').html(payload['reg_txn'])
                $('#status_txn').html(payload['status_txn'])
                time = payload['time_status_txn']
                dateObj = new Date(time * 1000);
                time = dateObj.toUTCString();
                $('#time_status_txn').html(time)
            }
        }
        xmlHttp.open("GET", url + "/account_info?address=" + address, true); // true for asynchronous 
        xmlHttp.send(null);
    });
})