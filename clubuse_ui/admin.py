from django.contrib import admin
from clubuse_ui.models import User, StoricoStati

# Register your models here.
admin.site.register(User)
admin.site.register(StoricoStati)
