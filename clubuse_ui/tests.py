from django.test import TestCase
import base64
import json
import requests
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from clubuse_ui.models import User
from clubuse_ui.asa_utils.tx_util import *
from algosdk.v2client.algod import AlgodClient
from algosdk import algod as algodv1
from algosdk.transaction import SignedTransaction
from django.core.serializers.json import DjangoJSONEncoder
from clubuse_ui.cripto_util import isPrivacyPayload


class RestAPITest(TestCase):
    def setUp(self):
        print("pass.... SETUP")

    def test_all(self):
        """Full test on api from registration to node switch"""
        print("skip test...")
        # acl = AlgodClient(params.algod_token, params.algod_address)
        # user_private_key, address = wallet_util.generate_algorand_keypair(
        #     print_log=False)

        # self.assertTrue(user_private_key is not None)
        # self.assertTrue(address is not None)

        # # REGISTER API
        # url = "http://localhost:8081/api/register"
        # payload = {"addr": address, "mobile": "123456789"}
        # headers = {"Content-type": "application/json"}
        # resp = requests.post(url, json=payload, headers=headers)
        # data = resp.json()
        # register_tx = data["txid"]
        # self.assertTrue(register_tx is not None)

        # # ORA CHIAMO OPTIN
        # url = "http://localhost:8081/api/optin_request"
        # resp = requests.post(url, json=payload, headers=headers)
        # data = resp.json()
        # txn1 = data["txn1"]
        # txn2 = data["txn2"]
        # txn1 = encoding.msgpack_decode(txn1)
        # txn1 = txn1.sign(user_private_key)
        # txn2 = encoding.msgpack_decode(txn2)
        # txn2 = txn2.sign(user_private_key)
        # self.assertIsInstance(txn1, SignedTransaction)
        # self.assertIsInstance(txn2, SignedTransaction)

        # # ORA INVIO LE TRANSAZIONI FIRMATE
        # url = "http://localhost:8081/api/send_trx_optin"
        # payload = {
        #     "signed_txn1": encoding.msgpack_encode(txn1),
        #     "signed_txn2": encoding.msgpack_encode(txn2),
        #     "addr": address,
        # }
        # resp = requests.post(url, json=payload, headers=headers)
        # data = resp.json()
        # state_tx = data["txid"]
        # self.assertTrue(state_tx is not None)
        # wait_for_confirmation(acl, state_tx)

        # # NOW CHECK INFO USER
        # url = "http://localhost:8081/api/user_info"
        # payload = {
        #     "addr": address,
        # }
        # resp = requests.post(url, json=payload, headers=headers)
        # data = resp.json()
        # opt_in = data["account"]["OPT_IN"]
        # opt_out = data["account"]["OPT_OUT"]
        # txn_assed_id = data["txn_assed_id"]
        # tx_reg_id = data["tx_reg_id"]
        # timestamp = data["timestamp"]
        # self.assertEqual(opt_in, 0)
        # self.assertEqual(opt_out, 1)
        # self.assertEqual(tx_reg_id, register_tx)
        # self.assertEqual(txn_assed_id, state_tx)
        # self.assertTrue(timestamp is not None)

        # # chiedi txn cambio stato
        # url = "http://localhost:8081/api/toggle_state"
        # payload = {
        #     "addr": address,
        # }
        # resp = requests.post(url, json=payload, headers=headers)
        # data = resp.json()
        # txn1 = data["txn1"]
        # txn2 = data[
        #     "txn2"]  # non la tocco, la lascio come risposta potrebbe controllare la firm il device
        # txn1 = encoding.msgpack_decode(txn1)
        # txn1 = txn1.sign(user_private_key)
        # self.assertIsInstance(txn1, SignedTransaction)

        # # ora invia le transazioni quindi cambia stato
        # url = "http://localhost:8081/api/send_trx_state"
        # payload = {
        #     "signed_txn1": encoding.msgpack_encode(txn1),
        #     "signed_txn2": txn2,
        # }
        # resp = requests.post(url, json=payload, headers=headers)
        # data = resp.json()
        # state_tx = data["txid"]
        # self.assertTrue(state_tx is not None)
        # wait_for_confirmation(acl, state_tx)

        # # NOW CHECK INFO USER
        # url = "http://localhost:8081/api/user_info"
        # payload = {
        #     "addr": address,
        # }
        # resp = requests.post(url, json=payload, headers=headers)
        # data = resp.json()
        # opt_in = data["account"]["OPT_IN"]
        # opt_out = data["account"]["OPT_OUT"]
        # txn_assed_id = data["txn_assed_id"]
        # tx_reg_id = data["tx_reg_id"]
        # timestamp = data["timestamp"]
        # self.assertEqual(opt_in, 1)
        # self.assertEqual(opt_out, 0)
        # self.assertEqual(tx_reg_id, register_tx)
        # self.assertEqual(txn_assed_id, state_tx)
        # self.assertTrue(timestamp is not None)
