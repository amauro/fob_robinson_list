from .api import *
from .views import *

# from django.shortcuts import render

# def error_page(request, status_code, **kwargs):
#     response = render(request, "%3d.html" % status_code, kwargs)
#     response.status_code = status_code
#     return response

# def make_error_page(code):
#     def f(request, *args, **kwargs):
#         print(args, kwargs)
#         return error_page(request, code, **kwargs)

#     return f

# page_not_found = make_error_page(404)
# server_error = make_error_page(500)
# permission_denied = make_error_page(403)
# bad_request = make_error_page(400)
