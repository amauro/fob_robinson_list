import base64
import json
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view
from clubuse_ui.algosdk_util import wallet_util, params, wallet_init
from clubuse_ui.models import User
from clubuse_ui.asa_utils.tx_util import *
from algosdk.v2client.algod import AlgodClient
from algosdk import algod as algodv1
from django.core.serializers.json import DjangoJSONEncoder
from clubuse_ui.cripto_util import isPrivacyPayload

STEP_SIZE = 100000  # 100K

#####
# API#
# https://bezkoder.com/django-rest-api/
# API IN-OUT JSON
#####


def _get_initial():
    opt_out_asset_id = int(params.opt_out_asset_id)
    opt_in_asset_id = int(params.opt_in_asset_id)
    fub_account = User.objects.get(username="attestator")
    fub_addr = fub_account.address
    # WITH FUB, I LOAD WALLET WORKAROUND
    fub_private_key = wallet_util.get_private_key(fub_account.username,
                                                  fub_account.username,
                                                  fub_addr)
    acl = AlgodClient(params.algod_token, params.algod_address)
    return opt_out_asset_id, opt_in_asset_id, fub_addr, fub_private_key, acl


# INPUT: addr (string) - mobile (string)
# OUTPUT txid (string) [la transazione con ci si è registrati, ovvero dove si trova il payload cifrato]
@api_view(["POST"])
def register_api(request):
    # params addr, mobile, username, password
    # ex. { "addr":"IPEKLK3JY4Q3ZJGRLH457Y2XY3WRHZMN3BCQ3VJ7YDPEKCTVZKZJNIIHF4","mobile":"test123"}

    _, _, fub_addr, fub_private_key, acl = _get_initial()
    data = JSONParser().parse(request)
    if "addr" not in data and "mobile" not in data:
        return JsonResponse(
            {
                "message":
                "Parametri obbligatori nel payload, addr e mobile, mancanti!"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    mobile = data["mobile"]
    addr = data["addr"]

    # CREATE USER SKIP
    if User.objects.filter(username=mobile).count() < 1:
        # user not created so create
        user = User.objects.create_user(
            username=mobile,
            password=mobile,
            address=addr,
            mob_phone=mobile,
        )
        user.save()

    # INIT MONEY
    txn = make_pay_transaction(acl, fub_addr, addr, 1000000)
    stxn = txn.sign(fub_private_key)
    try:
        print("sent using v2")
        acl.send_transaction(stxn)
        client = acl
    except:
        algod_token = acl.algod_token
        algod_address = acl.algod_address
        acl1 = algodv1.AlgodClient(algod_token, algod_address)
        acl1.send_transaction(stxn)
        print("sent using v1")
        client = acl1

    # registration payload on chain crypt
    msg = {"address": addr, "phone": mobile}
    txid = wallet_util.generate_and_send_privacy_payload(
        fub_private_key,
        fub_addr,
        addr,
        json.dumps(msg, sort_keys=True, indent=1, cls=DjangoJSONEncoder),
    )

    # ..da mobile le due richieste di opt in
    # dopo l'attestator manda il token default
    wait_for_confirmation(client, txid)
    return JsonResponse(
        {"txid": txid},
        status=status.HTTP_200_OK,
    )


# INPUT: addr (string)
# OUTPUT txn1 (string) [la prima transazione optin da firmare, encoding.msgpack_encode]
#        txn2 (string) [la seconda transazione optin da firmare, encoding.msgpack_encode]
@api_view(["POST"])
def get_optin(request):
    opt_out_asset_id, opt_in_asset_id, _, _, acl = _get_initial()
    data = JSONParser().parse(request)
    if "addr" not in data:
        return JsonResponse(
            {
                "message":
                "Parametro obbligatorio nel payload, addr , mancante!"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    addr = data["addr"]

    # https://github.com/algorand/py-algorand-sdk/blob/74459ae5b659420313b39394aba28ddb228acc84/algosdk/transaction.py
    # write to file
    txn1 = make_optin_asa(acl, addr, opt_in_asset_id)
    txn1 = encoding.msgpack_encode({"txn": txn1.dictify()})
    txn2 = make_optin_asa(acl, addr, opt_out_asset_id)
    txn2 = encoding.msgpack_encode({"txn": txn2.dictify()})
    return JsonResponse(
        {
            "txn1": txn1,
            "txn2": txn2
        },
        status=status.HTTP_200_OK,
    )


# INPUT: addr (string)
#        signed_txn1 (string) [la prima transazione firmata]
#        signed_txn2 (string) [la seconda transazione optin firmata]
# OUTPUT txid (string) [la transazione con cui è stato cambiato lo stato]
@api_view(["POST"])
def send_trx_optin(request):
    opt_out_asset_id, _, fub_addr, fub_private_key, acl = _get_initial()
    data = JSONParser().parse(request)
    if "signed_txn1" not in data and "signed_txn2" not in data and "addr" not in data:
        return JsonResponse(
            {
                "message":
                "Parametri obbligatori nel payload, signed_txn e addr, mancante!"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    signed_txn1 = data["signed_txn1"]
    signed_txn2 = data["signed_txn2"]
    signed_txn1 = encoding.msgpack_decode(signed_txn1)
    signed_txn2 = encoding.msgpack_decode(signed_txn2)
    addr = data["addr"]
    acl.send_transaction(signed_txn1,
                         headers={"content-type": "application/x-binary"})
    acl.send_transaction(signed_txn2,
                         headers={"content-type": "application/x-binary"})
    # check se chiami questa API con token no già ricevuto
    # receive asa from attestator account
    txid = transfer_asa(
        acl,
        fub_addr,
        addr,
        fub_private_key,
        1,
        opt_out_asset_id,
        skip_unusefull=True,
    )

    # data id, trx
    wait_for_confirmation(acl, txid)
    return JsonResponse(
        {"txid": txid},
        status=status.HTTP_200_OK,
    )


# INPUT: addr (string)
# OUTPUT txn1 (string) [la prima transazione per lo scambio del token DA FIRMARE, encoding.msgpack_encode]
#        txn2 (string) [la seconda transazione per lo scambio del token, già firmata dallo smart contract, encoding.msgpack_encode]
@api_view(["POST"])
def cambia_stato(request):
    opt_out_asset_id, opt_in_asset_id, _, _, acl = _get_initial()
    data = JSONParser().parse(request)
    if "addr" not in data:
        return JsonResponse(
            {
                "message":
                "Parametro obbligatorio nel payload, addr , mancante!"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    addr = data["addr"]
    info = wallet_util.getAccountInfo(addr)
    if info["OPT_IN"] == 1 and info["OPT_OUT"] == 0:
        from_asset_id = opt_in_asset_id
        to_asset_id = opt_out_asset_id
    elif info["OPT_IN"] == 0 and info["OPT_OUT"] == 1:
        from_asset_id = opt_out_asset_id
        to_asset_id = opt_in_asset_id
    else:
        return JsonResponse(
            {"message": info},
            status=status.HTTP_406_NOT_ACCEPTABLE,
        )

    program = open("opt_in_out_sc.lsig", "rb").read()
    sc_addr = transaction.LogicSig(program).address()

    txns = generate_optin_group_trx(
        acl,
        addr,
        sc_addr,
        program,
        from_asset_id,
        to_asset_id,
    )

    txn1 = txns[0]
    txn1 = encoding.msgpack_encode({"txn": txn1.dictify()})
    txn2 = txns[1]
    txn2 = encoding.msgpack_encode(txn2)
    return JsonResponse(
        {
            "txn1": txn1,
            "txn2": txn2
        },
        status=status.HTTP_200_OK,
    )


# INPUT txn1 (signed_txn1) [la prima transazione per lo scambio del token FIRMATA]
#       txn2 (string) [la seconda transazione per lo scambio del token, già firmata dallo smart contract]
@api_view(["POST"])
def send_trx_state(request):
    _, _, _, _, acl = _get_initial()
    data = JSONParser().parse(request)
    if "signed_txn1" not in data and "signed_txn2" not in data:
        return JsonResponse(
            {
                "message":
                "Parametri obbligatori nel payload, signed_txn1 e signed_txn2, mancante!"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    signed_txn1 = data["signed_txn1"]
    signed_txn2 = data["signed_txn2"]
    signed_txn1 = encoding.msgpack_decode(signed_txn1)
    signed_txn2 = encoding.msgpack_decode(signed_txn2)

    signedGroup = []
    signedGroup.append(signed_txn1)
    signedGroup.append(signed_txn2)
    # send them over network
    txid = acl.send_transactions(signedGroup)

    # data id, trx
    wait_for_confirmation(acl, txid)
    return JsonResponse(
        {"txid": txid},
        status=status.HTTP_200_OK,
    )


# INPUT: addr (string)
# OUTPUT: json
#     "account": info,
#     "txn_assed_id": txn_assed_id,
#     "timestamp": timestamp,
#     "tx_reg_id": tx_reg_id,
@api_view(["POST"])
def get_user_info(request):
    # _, _, _, _, _ = _get_initial()
    data = JSONParser().parse(request)
    if "addr" not in data:
        return JsonResponse(
            {
                "message":
                "Parametro obbligatorio nel payload, addr , mancante!"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    addr = data["addr"]
    info = wallet_util.getAccountInfo(addr)
    txn_assed_id, timestamp, tx_reg_id = _getAccountInfo(addr)

    return JsonResponse(
        {
            "account": info,
            "txn_assed_id": txn_assed_id,
            "timestamp": timestamp,
            "tx_reg_id": tx_reg_id,
        },
        status=status.HTTP_200_OK,
    )
    # Wallet - Registered with transaction ID - Status - Last Modified - Transaction Id


# LA PAGINA DI VISTA E' TOTALMENTE SU MOBILE, MA PER IL MOMENTO POTREBBE SALTARE


def _getAccountInfo(address):
    algod_client = algodv1.AlgodClient(params.algod_token,
                                       params.algod_address)
    last_round = algod_client.status().get("lastRound")
    # attestator default address
    fub_addr = User.objects.filter(username="attestator").first().address

    txns_list = []
    for i_step in range(0, last_round, STEP_SIZE):
        # print(i_step, i_step + step)
        last_step = i_step + STEP_SIZE
        if last_step > last_round:
            last_step = last_round
            txns = algod_client.transactions_by_address(
                address,
                first=i_step,
                last=last_step,
                limit=None,
                from_date=None,
                to_date=None,
            )
            txns_list.append(txns)
    round_block = -1
    timestamp = None
    tx_id = None
    tx_reg_id = None
    for txns in txns_list:
        if "transactions" in txns:
            for txn in txns["transactions"]:
                if (txn["type"] == "axfer" and txn["curxfer"]["amt"] > 0
                        and txn["round"] > round_block):
                    round_block = txn["round"]
                    print(round_block)
                if (txn["type"] == "axfer" and txn["curxfer"]["amt"] > 0
                        and txn["round"] == round_block and
                    (txn["from"] == address or txn["from"] == fub_addr)):
                    tx_id = txn["tx"]
                if txn["type"] == "pay" and tx_reg_id is None and "noteb64" in txn:
                    payload = base64.b64decode(txn["noteb64"]).decode()
                    if isPrivacyPayload(payload):
                        tx_reg_id = txn["tx"]

    if round_block > -1:
        block = algod_client.block_info(round_block)
        timestamp = block["timestamp"]

    return tx_id, timestamp, tx_reg_id
