import json
import logging
import base64
import nacl
import csv
import datetime
import time

from django.http import FileResponse
from nacl.signing import SigningKey, VerifyKey
from nacl.public import PrivateKey, Box, PublicKey
import nacl.bindings as b

from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from django.core.serializers.json import DjangoJSONEncoder

from django.http import HttpResponseForbidden
from django.conf import settings
from ratelimit.decorators import ratelimit as original_ratelimit
from django.contrib import messages
from algosdk.encoding import decode_address
from algosdk.util import sign_bytes, verify_bytes
from algosdk.mnemonic import from_private_key, to_public_key, to_private_key

from django.contrib.staticfiles.templatetags import staticfiles
from django.shortcuts import render, redirect
from django.db import transaction as db_transaction
from django.db.models import Q
from django.template import loader
from django.http import HttpResponse
from django.urls import reverse
from algosdk import account, encoding, mnemonic
from algosdk.v2client.algod import AlgodClient
from django.contrib.auth import login, authenticate
from django.views.generic import ListView, DetailView
from django.contrib.auth import views as auth_views

from django.views.generic.edit import FormView
from clubuse_ui.forms import SignUpForm
from clubuse_ui.models import User
from clubuse_ui.cripto_util import (
    generate_privacy_payload,
    read_privacy_payload,
    isPrivacyPayload,
)
from bootstrap_italia_template.widgets import (
    BootstrapItaliaRadioWidget,
    BootstrapItaliaSelectWidget,
)

from algosdk import kmd

from clubuse_ui.algosdk_util import wallet_util, params, wallet_init, asc_test
from clubuse_ui.asa_utils.tx_util import *

opt_out_asset_id = int(params.opt_out_asset_id)
opt_in_asset_id = int(params.opt_in_asset_id)
STEP_SIZE = 100000  # 100K


def index(request):
    if request.user.is_authenticated:
        text = wallet_util.getAccountInfo(request.user.address)
        txn_assed_id, timestamp, tx_reg_id = _getAccountInfo(
            request.user.address)
        try:
            print("*********mnemonic and key for debug*******")
            print(wallet_util.get_mnemonic_priv_test(request.user))
        except:
            pass

        pk = request.session.get("pk", None)
        mnemonic = request.session.get("mnemonic", None)
        # empty values
        request.session["pk"] = None
        request.session["mnemonic"] = None
        if request.session.get("step", None) == 4:
            request.session["step"] = None
    else:
        text = None
        pk = None
        mnemonic = None
        timestamp = None
        txn_assed_id = None
        tx_reg_id = None
    template = loader.get_template("index.html")
    context = {
        "assets": text,
        "pk": pk,
        "mnemonic": mnemonic,
        "timestamp": timestamp,
        "txn_assed_id": txn_assed_id,
        "trx_id": tx_reg_id,
    }
    return HttpResponse(template.render(context, request))


def message_success(request):
    import time

    # time.sleep(3)  # 3 seconds
    step = request.session.get("step", None)
    step_next = 1
    if step == 1:
        messages.success(request, "Assets created")
        messages.success(request, "OPTIN TOKEN: 7")
        messages.success(request, "OPTOUT TOKEN: 8")
        step_next = 2
    if step == 2:
        messages.success(request, "Smart contract compiled")
        step_next = 3
        request.session["step"] = step_next
        template = loader.get_template("sc.html")
        return HttpResponse(template.render({}, request))
    if step == 3:
        messages.success(request, "Smart Contract Deployed")
        messages.success(
            request,
            "Address: GWOSY6JVKEWBU2RA3BJ3QHBAST34RSUOKV3OL7F7B5XTQA7ILGA6Q2WEDM",
        )
        step_next = 4
    request.session["step"] = step_next
    return redirect(reverse("index"))


class CustomLoginView(auth_views.LoginView):
    def form_valid(self, form):
        return super().form_valid(form)


class RegisterView(FormView):
    form_class = SignUpForm
    template_name = "registration.html"
    navbar_item = "register"

    def get_success_url(self):
        return reverse("index")

    def form_valid(self, form):
        with db_transaction.atomic():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            form.cleaned_data.get("")

            # create wallet
            kcl_primary = kmd.KMDClient(params.kmd_token, params.kmd_address)
            kcl1 = kmd.KMDClient(params.kmd_token1, params.kmd_address1)
            kcl2 = kmd.KMDClient(params.kmd_token2, params.kmd_address2)
            kcl3 = kmd.KMDClient(params.kmd_token3, params.kmd_address3)
            kcl4 = kmd.KMDClient(params.kmd_token4, params.kmd_address4)
            if "kcl1" in username:
                kcl = kcl1
            else:
                kcl = kcl_primary
            address, pk, mnemonic = wallet_util.create_wallet(username,
                                                              raw_password,
                                                              withpk=True,
                                                              kcl=kcl)

            # now send payload in chain
            # wallet_init.initMoney(receiver=address)
            # initialize money in just created wallet
            acl = AlgodClient(params.algod_token, params.algod_address)
            fub_account = User.objects.get(username="attestator")
            fub_addr = fub_account.address
            # WITH FUB, I LOAD WALLET WORKAROUND
            fub_private_key = wallet_util.get_private_key(
                fub_account.username, fub_account.username, fub_addr)
            txn = make_pay_transaction(acl, fub_addr, address, 1000000)
            stxn = txn.sign(fub_private_key)
            acl.send_transaction(stxn)

            address_attestator = User.objects.get(
                username="attestator").address
            msg = {
                "address": address,
                "phone": form.cleaned_data.get("mob_phone")
            }
            wallet_util.generate_and_send_privacy_payload(
                fub_private_key,
                address_attestator,
                address,
                json.dumps(msg,
                           sort_keys=True,
                           indent=1,
                           cls=DjangoJSONEncoder),
            )

            # now save user
            user = authenticate(username=username, password=raw_password)
            user.address = address
            user.save()
            # save data pk and mnemonic in session
            self.request.session["pk"] = pk
            self.request.session["mnemonic"] = mnemonic
            # now login
            login(self.request, user)
        return super().form_valid(form)


def optin_user_asa(request):
    if not request.user.is_authenticated:
        return HttpResponseForbidden("403 Forbidden")
    input_sentence = request.POST.get("priv_mnem")
    choice = request.POST.get("choice", "priv_key")
    # optin assets
    acl = AlgodClient(params.algod_token, params.algod_address)
    account_info = acl.account_info(request.user.address)
    for asset_id in (opt_in_asset_id, opt_out_asset_id):
        holding = None
        if "assets" in account_info:
            for asset in account_info["assets"]:
                if asset["asset-id"] == asset_id:
                    holding = True
                    break

        if not holding:
            if choice == "mnemonic":
                user_private_key = mnemonic.to_private_key(input_sentence)
            else:
                user_private_key = input_sentence
            amount = wallet_util.getAccountInfo(request.user.address,
                                                all_info=True)["amount"]
            if amount < 1000:  # less than 1 Algo, so just created
                wallet_init.initMoney(receiver=request.user.address)
            optin_asa(acl, request.user.address, user_private_key, asset_id)

    text = wallet_util.getAccountInfo(request.user.address)
    if text["OPT_IN"] == 0 and text["OPT_OUT"] == 0:
        # BLACK BOX SUPPOSE NO VALIDATION
        # transfer Opt-out to the user
        fub_account = User.objects.get(username="attestator")
        fub_addr = fub_account.address

        # WITH FUB, I LOAD WALLET WORKAROUND
        fub_private_key = wallet_util.get_private_key(fub_account.username,
                                                      fub_account.username,
                                                      fub_addr)
        try:
            transfer_asa(
                acl,
                fub_addr,
                request.user.address,
                fub_private_key,
                1,
                opt_out_asset_id,
            )  # OPT-OUT to USER
            template = loader.get_template("index.html")
        except:
            template = loader.get_template("error_page.html")
    else:
        # GIA AUTENTICATO O DATO INCONSISTENTE
        template = loader.get_template("error_page.html")
    text = wallet_util.getAccountInfo(request.user.address)
    txn_assed_id, timestamp, trx_reg_id = _getAccountInfo(request.user.address)
    context = {
        "assets": text,
        "timestamp": timestamp,
        "txn_assed_id": txn_assed_id,
        "trx_id": trx_reg_id,
    }
    return HttpResponse(template.render(context, request))


def opt_out(request):
    start = time.time()
    if not request.user.is_authenticated:
        return HttpResponseForbidden("403 Forbidden")
    input_sentence = request.POST.get("priv_mnem")
    choice = request.POST.get("choice", "priv_key")
    # # Opt_IN user
    # transfer Opt-out to the user
    acl = AlgodClient(params.algod_token, params.algod_address)
    if choice == "mnemonic":
        user_private_key = mnemonic.to_private_key(input_sentence)
    else:
        user_private_key = input_sentence
    program = open("opt_in_out_sc.lsig", "rb").read()
    sc_addr = transaction.LogicSig(program).address()
    try:
        generate_optin_group(
            acl,
            request.user.address,
            user_private_key,
            sc_addr,
            program,
            opt_out_asset_id,
            opt_in_asset_id,
            request,
        )
        template = loader.get_template("index.html")
    except Exception as e:
        print(e)
        template = loader.get_template("error_page.html")
    text = wallet_util.getAccountInfo(request.user.address)
    txn_assed_id, timestamp, trx_reg_id = _getAccountInfo(request.user.address)
    context = {
        "assets": text,
        "timestamp": timestamp,
        "txn_assed_id": txn_assed_id,
        "trx_id": trx_reg_id,
    }
    end = time.time() - start
    print("finished****")
    print(end)
    return HttpResponse(template.render(context, request))


def opt_in(request):
    start = time.time()
    if not request.user.is_authenticated:
        return HttpResponseForbidden("403 Forbidden")
    input_sentence = request.POST.get("priv_mnem", "")
    choice = request.POST.get("choice", "priv_key")
    # # Opt_IN user
    # transfer Opt-out to the user
    acl = AlgodClient(params.algod_token, params.algod_address)
    if choice == "mnemonic":
        user_private_key = mnemonic.to_private_key(input_sentence)
    else:
        user_private_key = input_sentence
    program = open("opt_in_out_sc.lsig", "rb").read()
    sc_addr = transaction.LogicSig(program).address()

    try:
        generate_optin_group(
            acl,
            request.user.address,
            user_private_key,
            sc_addr,
            program,
            opt_in_asset_id,
            opt_out_asset_id,
            request,
        )
        template = loader.get_template("index.html")
    except Exception as e:
        print(e)
        template = loader.get_template("error_page.html")
    text = wallet_util.getAccountInfo(request.user.address)
    txn_assed_id, timestamp, trx_reg_id = _getAccountInfo(request.user.address)
    context = {
        "assets": text,
        "timestamp": timestamp,
        "txn_assed_id": txn_assed_id,
        "trx_id": trx_reg_id,
    }
    end = time.time() - start
    print("finished****")
    print(end)
    return HttpResponse(template.render(context, request))


class TransactionsView(ListView):
    model = User
    template_name = "transactions.html"
    paginate_by = 20
    navbar_item = "transactions"

    def get_queryset(self):
        transaction_id = self.request.GET.get("transaction", None)
        init_block = self.request.GET.get("init_block", None)
        final_block = self.request.GET.get("final_block", None)
        type_tx = self.request.GET.get("type", None)
        if self.request.user.is_authenticated:
            wallet_address = self.request.GET.get("wallet",
                                                  self.request.user.address)
        else:
            wallet_address = self.request.GET.get("wallet", None)
        try:
            if self.request.user.is_authenticated:
                return _getTrxList(
                    address=wallet_address,
                    transaction_id=transaction_id,
                    init_block=init_block,
                    final_block=final_block,
                    type_tx=type_tx,
                )
            else:
                return _getTrxList(
                    transaction_id=transaction_id,
                    init_block=init_block,
                    final_block=final_block,
                    type_tx=type_tx,
                )
        except:
            print("Invalid search parameters !!!")
            return []

    def get_context_data(self, **kwargs):
        context = super(TransactionsView, self).get_context_data(**kwargs)
        context["transaction"] = self.request.GET.get("transaction")
        context["init_block"] = self.request.GET.get("init_block")
        context["final_block"] = self.request.GET.get("final_block")
        context["type"] = self.request.GET.get("type")
        if self.request.user.is_authenticated:
            wallet_address = self.request.GET.get("wallet",
                                                  self.request.user.address)
        else:
            wallet_address = self.request.GET.get("wallet", None)
        context["wallet"] = wallet_address
        return context


def _getTrxList(address=None,
                transaction_id=None,
                init_block=None,
                final_block=None,
                type_tx=None):
    algod_client = algodv1.AlgodClient(params.algod_token,
                                       params.algod_address)

    last_round = algod_client.status().get("lastRound")
    initial_round = last_round - 100

    if init_block:
        initial_round = int(init_block)
    if final_block:
        last_round = int(final_block)

    # if transaction_id is not None:
    #     #indexer isn't running, this call is disabled
    #     txns = algod_client.transaction_by_id(transaction_id)
    txns_list = []
    if address:
        for i_step in range(0, last_round, STEP_SIZE):
            last_step = i_step + STEP_SIZE
            if last_step > last_round:
                last_step = last_round
            txns = algod_client.transactions_by_address(
                address,
                first=i_step,
                last=last_step,
                limit=10**9,
                from_date=None,
                to_date=None,
            )
            txns_list.append(txns)
    else:
        # get last tean
        txns = {"transactions": []}
        for i in range(initial_round, last_round):
            block = algod_client.block_info(i)
            txns_tmp = block["txns"]
            if txns_tmp:
                txns_tmp = txns_tmp["transactions"]
            for t in txns_tmp:
                txns["transactions"].append(t)
        txns_list.append(txns)
    payloads = []
    for txns in txns_list:
        if "transactions" in txns:
            for txn in txns["transactions"]:
                if ((not transaction_id or transaction_id in txn["tx"])
                        and (not init_block or int(init_block) <= txn["round"])
                        and
                    (not final_block or int(final_block) > txn["round"])
                        and (not type_tx or type_tx == txn["type"])):
                    item = {}
                    if "noteb64" in txn:
                        item["payload"] = base64.b64decode(
                            txn["noteb64"]).decode()
                    item["sender"] = txn["from"]
                    item["txn_id"] = txn["tx"]
                    item["all"] = txn
                    payloads.append(item)
    return payloads


def readPayload(request):
    if not request.user.is_authenticated:
        return HttpResponseForbidden("403 Forbidden")
    input_sentence = request.POST.get("priv_mnem")
    choice = request.POST.get("choice", "priv_key")
    enc_payload = request.POST.get("enc_payload", "")
    tx_id = request.POST.get("tx_id", "")
    signing_address = request.POST.get("wallet_address", "")
    if choice == "mnemonic":
        user_private_key = mnemonic.to_private_key(input_sentence)
    else:
        user_private_key = input_sentence

    is_attestator = False
    if request.user.username == "attestator":
        is_attestator = True
    decrypted = read_privacy_payload(enc_payload,
                                     signing_address,
                                     user_private_key,
                                     attestator=is_attestator)
    print("decrypted******")
    print(decrypted)
    print(tx_id)
    request.session["decrypted"] = decrypted
    request.session["tx_id"] = tx_id

    return redirect(request.META.get("HTTP_REFERER") + "#decrypted")


def _getAccountInfo(address):
    algod_client = algodv1.AlgodClient(params.algod_token,
                                       params.algod_address)
    last_round = algod_client.status().get("lastRound")
    # attestator default address
    fub_addr = User.objects.filter(username="attestator").first().address

    txns_list = []
    for i_step in range(0, last_round, STEP_SIZE):
        # print(i_step, i_step + step)
        last_step = i_step + STEP_SIZE
        if last_step > last_round:
            last_step = last_round
            txns = algod_client.transactions_by_address(
                address,
                first=i_step,
                last=last_step,
                limit=None,
                from_date=None,
                to_date=None,
            )
            txns_list.append(txns)
    round_block = -1
    timestamp = None
    tx_id = None
    tx_reg_id = None
    for txns in txns_list:
        if "transactions" in txns:
            for txn in txns["transactions"]:
                if (txn["type"] == "axfer" and txn["curxfer"]["amt"] > 0
                        and txn["round"] > round_block):
                    round_block = txn["round"]
                    print(round_block)
                if (txn["type"] == "axfer" and txn["curxfer"]["amt"] > 0
                        and txn["round"] == round_block and
                    (txn["from"] == address or txn["from"] == fub_addr)):
                    tx_id = txn["tx"]
                if txn["type"] == "pay" and tx_reg_id is None and "noteb64" in txn:
                    payload = base64.b64decode(txn["noteb64"]).decode()
                    if isPrivacyPayload(payload):
                        tx_reg_id = txn["tx"]

    if round_block > -1:
        block = algod_client.block_info(round_block)
        timestamp = block["timestamp"]

    return tx_id, timestamp, tx_reg_id


def account_info(request):
    retPayload = {}
    address = request.GET.get("address", None)
    if request.user.is_authenticated and address is not None:
        tx_id, timestamp, tx_reg_id = _getAccountInfo(address)
        retPayload["reg_txn"] = tx_reg_id
        retPayload["time_status_txn"] = timestamp
        retPayload["status_txn"] = tx_id

    return HttpResponse(json.dumps(retPayload),
                        content_type="application/json")


class SubscriberList(ListView):
    model = User
    navbar_item = "subscribers"
    paginate_by = 15
    template_name = "subscribers.html"

    def get_queryset(self):
        retlist = []
        username = self.request.GET.get("username", "")
        address = self.request.GET.get("address", "")
        phone = self.request.GET.get("phone", "")
        for u in User.objects.filter(
                username__icontains=username,
                address__icontains=address,
                mob_phone__icontains=phone,
        ).all():
            if u.username == "attestator":
                continue
            num = _getstate(u.address)
            state = _num_to_status(num)
            item = {
                "username": u.username,
                "address": u.address,
                "mob_phone": u.mob_phone,
                "state": state,
                "status_code": num,
            }
            retlist.append(item)
        # test many iterations: di default utenti circa 20-> x 1000
        # for i in range(1, 1000):
        #     for u in User.objects.all():
        #         if u.username == "attestator":
        #             continue
        #         num, state = _getstate(u.address)
        return retlist

    def get_context_data(self, **kwargs):
        context = super(SubscriberList, self).get_context_data(**kwargs)
        context["registered"] = User.objects.all().count() - 1
        context["url"] = settings.URL_ROOT
        context["username"] = self.request.GET.get("username", "")
        context["address"] = self.request.GET.get("address", "")
        context["phone"] = self.request.GET.get("phone", "")
        return context


def _num_to_status(num):
    if num == -2:
        return "To be authenticated"
    elif num == -1:
        return "Inconsistent value"
    elif num == 0:
        return "OPT OUT"
    elif num == 1:
        return "OPT IN"
    else:
        return "Inconsistent value"


def _getstate(address):
    payload = wallet_util.getAccountInfo(address)
    num = -1
    opt_in = payload["OPT_IN"]
    opt_out = payload["OPT_OUT"]
    if opt_in == 0 and opt_out == 0:
        num = -2
    elif opt_in == 1 and opt_out == 0:
        num = 1
    elif opt_in == 0 and opt_out == 1:
        num = 0
    else:
        print(opt_in, opt_out, address)
    return num


def _get_addr_from_bc(mob_phone):
    # get whole bc
    transactions = _getTrxList(init_block="0")
    print("*******************")
    fub_account = User.objects.get(username="attestator")
    fub_addr = fub_account.address
    # WITH FUB, I LOAD WALLET WORKAROUND
    fub_private_key = wallet_util.get_private_key(fub_account.username,
                                                  fub_account.username,
                                                  fub_addr)
    # read back TODO
    for txn in transactions:
        if "payload" in txn and txn["payload"] != "tax":
            # print(txn["payload"])
            decrypted = read_privacy_payload(txn["payload"],
                                             fub_addr,
                                             fub_private_key,
                                             attestator=True)
            if (decrypted["phone"] == "16b6d25d7528d5c8143e863a19dcdf9d"
                ):  # phone for test
                print("found******")
                print(decrypted["address"])
    return User.objects.all().first().address


def confronta_csv(request):
    if (not request.user.is_authenticated or not request.user.is_staff
            or not request.user.is_superuser):
        return HttpResponseForbidden("403 Forbidden")
    try:
        with db_transaction.atomic():
            reader = csv.DictReader(
                decode_utf8(request.FILES["elenco_rpo_priv"]),
                delimiter=",",
                quotechar='"',
            )
            current_res = []
            current_res.append(["NUMERO", "CONSENSO"])
            for row in reader:
                mob_phone = row["NUMERO"]
                # OVERRIDE WITH TEST, address from bc
                address_from_db = User.objects.get(mob_phone=mob_phone).address
                # address_from_bc = _get_addr_from_bc(mob_phone)
                consenso = _getstate(address_from_db)
                current_res.append([mob_phone, consenso])

            nomefile = "%s_%s.csv" % (
                request.FILES["elenco_rpo_priv"].name,
                datetime.datetime.now().strftime("%Y-%m-%d"),
            )
            return get_csv_response(current_res, nomefile)
    except User.DoesNotExist:
        logging.exception(
            "errore parsing CSV, presente un numero di telefono non corrispondente a nessun utente"
        )
        messages.error(request, "Error reading file")
        messages.error(request,
                       "The phone number is not present: %s" % (mob_phone))
        return redirect(reverse("index"))
    except:
        logging.exception("errorr parsing CSV")
        messages.error(request, "Error reading file")
        return redirect(reverse("index"))


def get_csv_response(results, nomefile):
    """
    Funzione che, dato un QuerySet 'results', restituisce un oggetto StreamingHttpResponse
    con un file csv contenente i dati in 'results'.
    Il nome del file è dato da 'nomefile', mentre 'line_generator' è la funzione (o il generatore) che processa results.
    """
    def generate_csv(w):
        # yield codecs.BOM_UTF8  # forza utf8
        for row in results:
            yield w.writerow(row)

    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer)
    response = FileResponse(
        generate_csv(writer),
        content_type="text/csv",
        as_attachment=True,
        filename=nomefile,
    )
    response["Content-Disposition"] = "attachment; filename=" + nomefile + ";"
    return response


def decode_utf8(input_iterator):
    for l in input_iterator:
        yield l.decode("utf-8")


class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


# utility
"""
docker run -it algorand/testnet -c './goal node start -d data; while pgrep algod -x; do sleep 2; done'
"""


def bootstrap_static_base(request):
    resp = HttpResponse(
        "window.__PUBLIC_PATH__ = " + json.dumps(staticfiles.static("fonts")),
        content_type="application/javascript",
    )
    return resp


def generate_account():
    private_key, address = account.generate_account()
    print("Private key:", private_key)
    print("Address:", address)

    # check if the address is valid
    if encoding.is_valid_address(address):
        print("The address is valid!")
    else:
        print("The address is invalid.")


def log_view(func, message=None, level=logging.INFO, log="views"):
    if message is None:
        message = "entering view %s" % func.__name__

    def decorated(request, *args, **kwargs):
        m = message(request) if callable(message) else message
        if m:
            logging.getLogger(log).log(level, m)
        return func(request, *args, **kwargs)

    return decorated


def ratelimit(**kwargs):
    kwargs.setdefault("group", "general-security")
    kwargs.setdefault("key", "ip")
    kwargs.setdefault("rate", "50/30m")
    kwargs.setdefault("block", True)
    kwargs.setdefault("block", True)
    kwargs.setdefault("method", ("POST", "PUT", "PATCH", "DELETE"))
    return original_ratelimit(**kwargs)
