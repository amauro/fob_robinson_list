import json
import os
import logging
import base64
import nacl
from nacl.signing import SigningKey, VerifyKey
from nacl.public import PrivateKey, Box, PublicKey
import nacl.bindings as b

from Crypto.Cipher import AES
from Crypto import Random
from django.core.serializers.json import DjangoJSONEncoder

from ratelimit.decorators import ratelimit as original_ratelimit
from django.contrib import messages
from algosdk.encoding import decode_address
from algosdk.util import sign_bytes, verify_bytes
from algosdk.mnemonic import from_private_key, to_public_key, to_private_key

# def test_encrypt(request):
#     private_key = wallet_util.get_wallet_info(
#         request.user.username, request.user.username, request.user.address
#     )
#     private_key = base64.b64decode(
#         bytes(private_key, "utf-8")
#     )  # ora ho la chiave privata ed25519_sk

#     msg = b"prova encrypt"

#     # converto da ed25519 a X25519 per poter fare cifratura
#     sk = b.crypto_sign_ed25519_sk_to_curve25519(private_key)

#     # https://pynacl.readthedocs.io/en/stable/public/#examples
#     sk_op1 = PrivateKey(sk)
#     pk_op1 = sk_op1.public_key
#     sk_op2 = PrivateKey.generate()
#     pk_op2 = sk_op2.public_key
#     op1_box = Box(sk_op1, pk_op2)
#     cipher_text = op1_box.encrypt(msg)
#     op2_box = Box(sk_op2, pk_op1)
#     op2_box.decrypt(cipher_text)
#     return op2_box.decrypt(cipher_text)


def _get_public_key(address):
    return decode_address(address)  # ora ho la chiave pubblica ed25519_pk


def _crypto_msg(msg):
    key = os.urandom(16)
    iv = os.urandom(16)
    key_iv = key + iv
    print(key_iv, "*******************")
    obj = AES.new(key, AES.MODE_CBC, iv)
    pad_msg = msg.rjust(((int)(len(msg) / 16) * 16 + 16), "&")
    ciphertext = obj.encrypt(pad_msg)
    return key_iv, base64.encodestring(ciphertext).decode("ascii")
    # obj2 = AES.new("This is a key123", AES.MODE_CBC, "This is an IV456")
    # obj2.decrypt(ciphertext)


def _decrypt_msg(msg, key, iv):
    obj = AES.new(key, AES.MODE_CBC, iv)
    return obj.decrypt(msg)


def encrypt_aes_key(public_key, sk, aes_key):
    # converto da ed25519 a X25519 per poter fare cifratura
    pk = b.crypto_sign_ed25519_pk_to_curve25519(public_key)
    sk = PrivateKey(sk)
    pk = PublicKey(pk)
    box = Box(sk, pk)
    return base64.encodestring(box.encrypt(aes_key)).decode("ascii")


def decrypt_aes_key(public_key, sk, aes_key):
    # converto da ed25519 a X25519 per poter fare cifratura
    # print(aes_key)
    sk = PrivateKey(sk)
    pk = PublicKey(b.crypto_sign_ed25519_pk_to_curve25519(public_key))
    box = Box(sk, pk)
    return box.decrypt(aes_key)


def generate_privacy_payload(private_key, address_attestator,
                             address_subscriber, msg):
    ret_payload = {}
    aes_key, cipher = _crypto_msg(msg)
    # stesso record di DB
    ret_payload["msg"] = cipher
    # parte in chiaro per identificare

    private_key = base64.b64decode(bytes(
        private_key, "utf-8"))  # ora ho la chiave privata ed25519_sk
    sk = b.crypto_sign_ed25519_sk_to_curve25519(private_key)
    ret_payload["subscriber"] = encrypt_aes_key(
        _get_public_key(address_subscriber), sk, aes_key)
    ret_payload["attestator"] = encrypt_aes_key(
        _get_public_key(address_attestator), sk, aes_key)
    return json.dumps(ret_payload)


def isPrivacyPayload(payload):
    valid = False
    try:
        payload = json.loads(payload)
        if payload["msg"] and payload["attestator"] and payload["subscriber"]:
            valid = True
    except:
        valid = False
    return valid


def read_privacy_payload(payload,
                         signing_address,
                         private_key,
                         attestator=False):
    clear_msg = payload
    try:
        payload = json.loads(payload)
        enc_msg = base64.decodestring(payload["msg"].encode("ascii"))
        if attestator:
            reader = "attestator"
        else:
            reader = "subscriber"
        aes_key = base64.decodestring(payload[reader].encode("ascii"))
        private_key = base64.b64decode(bytes(
            private_key, "utf-8"))  # ora ho la chiave privata ed25519_sk
        sk = b.crypto_sign_ed25519_sk_to_curve25519(private_key)
        aes_key = decrypt_aes_key(_get_public_key(signing_address), sk,
                                  aes_key)
        # now read msg
        clear_msg = _decrypt_msg(enc_msg, aes_key[:16],
                                 aes_key[16:]).decode("ascii")
        clear_msg = json.loads(clear_msg.replace("&", ""))
    except Exception as e:
        logging.exception("errore")
    return clear_msg
