from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid
from django_extensions.db import models as ext_models


class User(AbstractUser):
    address = models.CharField(max_length=64)
    mob_phone = models.CharField(unique=True, max_length=20, blank=True, null=True)


class StoricoStati(models.Model):
    id = models.IntegerField(primary_key=True)
    codifica = models.CharField(max_length=128)
    stato = models.CharField(max_length=4, null=True, blank=True, default=None)
    frontend_id = models.IntegerField(null=True, blank=True, default=None)
    stato_id = models.IntegerField(null=True, blank=True, default=None)
    mezzo_id = models.IntegerField(null=True, blank=True, default=None)
    data = models.DateTimeField(null=True, blank=True, default=None)
