<h1>Usage</h1>

<p>The most common usage of the Indexer is expect to be getting validated blocks from a local <code>algod</code> Algorand node, adding them to a <a href="https://www.postgresql.org/">PostgreSQL</a> database, and serving an API to make available a variety of prepared queries. Some users may wish to directly write SQL queries of the database.</p>

<p>Indexer works by fetching blocks one at a time, processing the block data, and loading it into a traditional database. There is a database abstraction layer to support different database implementations. In normal operation the service will run as a daemon and always requires access to a database.</p>

<p>As of April 2020, storing all the raw blocks is about 100 GB and the PostgreSQL database of transactions and accounts is about 1 GB. Much of that size difference is the Indexer ignoring cryptographic signature data; relying on <code>algod</code> to validate blocks. Dropping that, the Indexer can focus on the 'what happened' details of transactions and accounts.</p>

<p>There are two primary modes of operation:
* Database updater
* Read only</p>

<h3>Database updater</h3>

<p>In this mode the database will be populated with data fetched from an <a href="https://developer.algorand.org/docs/run-a-node/setup/types/#archival-mode">Algorand archival node</a>. Because every block must be fetched to bootstrap the database, the initial import for a ledger with a long history will take a while. If the daemon is terminated, it will resume processing wherever it left off.</p>

<p>You should use a process manager, like systemd, to ensure the daemon is always running. Indexer will continue to update the database as new blocks are created.</p>

<p>To start indexer as a daemon in update mode, provide the required fields:</p>

<pre><code>~$ algorand-indexer daemon --algodAddr yournode.com:1234 --algodToken token --genesis ~/path/to/genesis.json  --postgres "user=readonly password=YourPasswordHere {other connection string options for your database}"
</code></pre>

<p>Alternatively if indexer is running on the same host as the archival node, a simplified command may be used:</p>

<pre><code>~$ algorand-indexer daemon --algodAddr yournode.com:1234 -d /path/to/algod/data/dir --postgres "user=readonly password=YourPasswordHere {other connection string options for your database}"
</code></pre>

<h3>Read only</h3>

<p>It is possible to set up one daemon as a writer and one or more readers. The Indexer pulling new data from algod can be started as above. Starting the indexer daemon without $ALGORAND_DATA or -d/--algod/--algod-net/--algod-token will start it without writing new data to the database. For further isolation, a <code>readonly</code> user can be created for the database.</p>

<pre><code>~$ algorand-indexer daemon --no-algod --postgres "user=readonly password=YourPasswordHere {other connection string options for your database}"
</code></pre>

<p>The Postgres backend does specifically note the username "readonly" and changes behavior to avoid writing to the database. But the primary benefit is that Postgres can enforce restricted access to this user. This can be configured with:</p>

<pre><code>CREATE USER readonly LOGIN PASSWORD 'YourPasswordHere';
REVOKE ALL ON ALL TABLES IN SCHEMA public FROM readonly;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO readonly;
</code></pre>

<h2>Authorization</h2>

<p>When <code>--token your-token</code> is provided, an authentication header is required. For example:</p>

<pre><code>~$ curl localhost:8980/transactions -H "X-Indexer-API-Token: your-token"
</code></pre>

<h1>Systemd</h1>

<p><code>/lib/systemd/system/algorand-indexer.service</code> can be partially overridden by creating <code>/etc/systemd/system/algorand-indexer.service.d/local.conf</code>. The most common things to override will be the command line and pidfile. The overriding local.conf file might be this:</p>

<pre><code>[Service]
ExecStart=/usr/bin/algorand-indexer daemon --pidfile /var/lib/algorand/algorand-indexer.pid --algod /var/lib/algorand --postgres "host=mydb.mycloud.com user=postgres password=password dbname=mainnet"
PIDFile=/var/lib/algorand/algorand-indexer.pid

</code></pre>

<p>The systemd unit file can be found in source at <a href="misc/systemd/algorand-indexer.service">misc/systemd/algorand-indexer.service</a></p>

<p>Once configured, turn on your daemon with:</p>

<pre><code>sudo systemctl enable algorand-indexer
sudo systemctl start algorand-indexer
</code></pre>

<p>If you wish to run multiple indexers on one server under systemd, see the comments in <code>/lib/systemd/system/algorand-indexer@.service</code> or <a href="misc/systemd/algorand-indexer@.service">misc/systemd/algorand-indexer@.service</a></p>
