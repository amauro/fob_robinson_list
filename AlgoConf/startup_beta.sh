#!/bin/bash
echo 'update and switch to betanet'
# Update software with beta flag and specify betanet directory.
./update.sh -i -c beta -p ./ -d ./betanetdata -n
./goal node stop -d betanetdata
cp /code/AlgoConf/genesis.json ./betanetdata/.
ls ./betanetdata

echo '### Start configuration ###'
./goal node start -d betanetdata; 
./goal node status -d betanetdata; 
./goal kmd start -d betanetdata; 

ls ./betanetdata/devnet-v1.0

while pgrep algod -x; 
	#clear file each round UPDATE
	 > /code/AlgoConf/node_var
	#now save var
	echo 'algod_token' >> /code/AlgoConf/node_var
	cat ./betanetdata/algod.token >> /code/AlgoConf/node_var
	echo '' >> /code/AlgoConf/node_var

	echo 'algod_net' >> /code/AlgoConf/node_var
	cat ./betanetdata/algod.net >> /code/AlgoConf/node_var

	echo 'kmd_token' >> /code/AlgoConf/node_var
	cat ./betanetdata/kmd-v0.5/kmd.token >> /code/AlgoConf/node_var
	echo '' >> /code/AlgoConf/node_var

	echo 'kmd_net' >> /code/AlgoConf/node_var
	cat ./betanetdata/kmd-v0.5/kmd.net >> /code/AlgoConf/node_var
	
	do sleep 1000;
done

# START WEB APP
# python manage.py runserver 0.0.0.0:80
# apache2ctl -DFOREGROUND