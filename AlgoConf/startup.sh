#!/bin/bash
echo '### Start configuration ###'
# Update software with beta flag and specify betanet directory.
./update.sh -i -c beta -p ./ -d ./data -n -g betanet 
# Run the update script a second time to update goal
./update.sh -i -c beta -p ./ -d ./data -n -g betanet
# #run indexer
# /algo_indexer/algorand-indexer daemon --algod=./data

DIR="/blockchain/cu_net/"
if [ -d "$DIR" ]; then
  echo "Existing directory, so only start"
else
  echo "Not existing, so create network"
  ./goal network create -r /blockchain/cu_net -n private -t /rpo_template.json;
fi
echo '{ "GossipFanout": 4, "CadaverSizeTarget": 0, "EndpointAddress": "0.0.0.0:8080", "DNSBootstrapID": "", "EnableProfiler": true, "NetAddress": "127.0.0.1:2323" }' > /blockchain/cu_net/Primary/config.json
echo '{ "GossipFanout": 4, "CadaverSizeTarget": 0, "EndpointAddress": "0.0.0.0:43147", "DNSBootstrapID": "", "EnableProfiler": true }' > /blockchain/cu_net/Node1/config.json
echo '{ "GossipFanout": 4, "CadaverSizeTarget": 0, "EndpointAddress": "0.0.0.0:38835", "DNSBootstrapID": "", "EnableProfiler": true }' > /blockchain/cu_net/Node2/config.json
echo '{ "GossipFanout": 4, "CadaverSizeTarget": 0, "EndpointAddress": "0.0.0.0:33153", "DNSBootstrapID": "", "EnableProfiler": true }' > /blockchain/cu_net/Node3/config.json
echo '{ "GossipFanout": 4, "CadaverSizeTarget": 0, "EndpointAddress": "0.0.0.0:44763", "DNSBootstrapID": "", "EnableProfiler": true }' > /blockchain/cu_net/Node4/config.json

./goal network start -r /blockchain/cu_net;
echo '{"address": "0.0.0.0:7833"}' > /blockchain/cu_net/Primary/kmd-v0.5/kmd_config.json
echo '{"address": "0.0.0.0:40131"}' > /blockchain/cu_net/Node1/kmd-v0.5/kmd_config.json
echo '{"address": "0.0.0.0:40132"}' > /blockchain/cu_net/Node2/kmd-v0.5/kmd_config.json
echo '{"address": "0.0.0.0:40133"}' > /blockchain/cu_net/Node3/kmd-v0.5/kmd_config.json
echo '{"address": "0.0.0.0:40134"}' > /blockchain/cu_net/Node4/kmd-v0.5/kmd_config.json
echo 'Port configured!!!'
./goal kmd stop -d /blockchain/cu_net/Primary;
./goal kmd start -d /blockchain/cu_net/Primary;
./goal kmd stop -d /blockchain/cu_net/Node1;
./goal kmd start -d /blockchain/cu_net/Node1;
./goal kmd stop -d /blockchain/cu_net/Node2;
./goal kmd start -d /blockchain/cu_net/Node2;
./goal kmd stop -d /blockchain/cu_net/Node3;
./goal kmd start -d /blockchain/cu_net/Node3;
./goal kmd stop -d /blockchain/cu_net/Node4;
./goal kmd start -d /blockchain/cu_net/Node4;
# ./goal node start -d /blockchain/cu_net/Primary;

while true;
	if [ $(pgrep -c -x algod) -gt 0 ]
	then
		echo "algod is running";
	else
		echo "algod stopped";
		./goal network start -r /blockchain/cu_net;
	fi
	#5 kmd nodes
	if [ $(pgrep -c -x kmd) -gt 4 ]
	then
		echo "kmd is running";
	else
		echo "kmd stopped";
		./goal kmd start -d /blockchain/cu_net/Primary;
		./goal kmd start -d /blockchain/cu_net/Node1;
		./goal kmd start -d /blockchain/cu_net/Node2;
		./goal kmd start -d /blockchain/cu_net/Node3;
		./goal kmd start -d /blockchain/cu_net/Node4;
	fi
	do sleep 2;
done