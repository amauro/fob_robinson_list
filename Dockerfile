FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
ADD ./ClubUse /code/
COPY . /code/
#from algorand add startup.sh in ....